<?php
// $Id$

/**
 * @file
 * Demonstrate basic module mkb.
 */

/**
 * Implements hook_form_alter().
 */
function mkb_form_alter(&$form, &$form_state, $form_id) {
if ($form_id == 'organisation_node_form') {
  $old_form = $form['field_parent_organisation']['#language'];
  if (isset($language)) {                               // Kører flere gange og kun første gang må bruges
//    $options = org_options(1);
    $options = array( "1" => "Første", "2" => "Anden");
    $form['field_parent_organisation'] = array(
        '#type' => 'select',
        '#title' => $old_form['#title'],
        '#default_value' => $old_form['#default_value'][0],
        '#description' => t('Choose parent organisation'),
        '#required' => FALSE,
        '#options' => $options
        );
/*
    function org_options($this_nid) {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_parent_organisation', 'p', 'n.nid = p.entity_id');
      $query
      ->fields('n', array('title', 'nid'))
      ->fields('p', array('field_parent_organisation_target_id'))
      ->condition('n.type', 'organisation')
      ->addTag('node_access');
      $result = $query->execute();
    
      foreach($result as $row) {
        $nid = $row -> nid;                                                                           // This organisation's id
        $pid = $row -> field_parent_organisation_target_id;                       // This organisation's parent_id
        if (isset($pid)) $parent_array[$nid] = $pid;                                            // Parent_id in array
        else $parent_array[$nid] = null;                                                             // or null
        $name[$nid] = $row->title;
      }
      $options = array();
      while (list($key,$val) = each($parent_array)) {
        $n_parents = count_parents(0, $key, $parent_array);
        $level_name = '';
        for ($x=0; $x<$n_parents; $x++) $level_name .= '- ';
        $level_name .= $name[$key];
        $options[$key] = $level_name;
      }
      var_dump($options);
      return $options;
    }
    
    function count_parents($n_parents, $nid, $parent_array) {
      if (isset($parent_array[$nid])) {
        $n_parents += count_parents($n_parents, $parent_array[$nid], $parent_array);
        $n_parents ++;
      }
      return $n_parents;
    }
*/  
    }
  }
}
