<?php
/**
 * @file
 * mkb_org.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mkb_org_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_adm|node|organisation|form';
  $field_group->group_name = 'group_org_adm';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Administrative fields',
    'weight' => '2',
    'children' => array(
      0 => 'field_org_name',
      1 => 'field_org_path',
      2 => 'field_partner_number',
      3 => 'field_new_author',
      4 => 'field_mapping',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-org-adm field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_org_adm|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_opt|node|organisation|form';
  $field_group->group_name = 'group_org_opt';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Optional fields',
    'weight' => '1',
    'children' => array(
      0 => 'field_contact_person',
      1 => 'field_email',
      2 => 'field_phone',
      3 => 'field_street_name',
      4 => 'field_house_number',
      5 => 'field_zip',
      6 => 'field_city',
      7 => 'field_region',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Optional fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-org-opt field-group-fieldset',
        'description' => 'These fields are inhereted from the parent field if they are left empty',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_org_opt|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_rec|node|organisation|form';
  $field_group->group_name = 'group_org_rec';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Required fields',
    'weight' => '0',
    'children' => array(
      0 => 'field_short_name',
      1 => 'field_parent_organisation',
      2 => 'field_url',
      3 => 'field_country',
      4 => 'field_description',
      5 => 'field_organisation_type',
      6 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-org-rec field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_org_rec|node|organisation|form'] = $field_group;

  return $export;
}
