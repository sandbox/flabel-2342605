<?php
/**
 * @file
 * mkb_org.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function mkb_org_taxonomy_default_vocabularies() {
  return array(
    'organisation_type' => array(
      'name' => 'Organisation type',
      'machine_name' => 'organisation_type',
      'description' => 'Organisation type used in organisation content type',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
