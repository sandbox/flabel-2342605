<?php
/**
 * @file
 * mkb_org.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mkb_org_user_default_roles() {
  $roles = array();

  // Exported role: project member.
  $roles['project member'] = array(
    'name' => 'project member',
    'weight' => 3,
  );

  return $roles;
}
