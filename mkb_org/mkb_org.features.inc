<?php
/**
 * @file
 * mkb_org.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mkb_org_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mkb_org_views_api($module = NULL, $api = NULL) {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'mkb_org'),
    'template path' => drupal_get_path('module', 'mkb_org') . '/theme',
  );
}

/**
 * Implements hook_node_info().
 */
function mkb_org_node_info() {
  $items = array(
    'organisation' => array(
      'name' => t('Organisation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
