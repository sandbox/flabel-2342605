<div id="hor-zebra">
<h2><?php print $call_main->title;?></h2>
<h1><?php if(isset($project_info['title'])) print $project_info['title'];?></h1>
<p><b>Id </b><?php print $group->nid;?></p>
<p><b>Acronym </b><?php if(isset($project_info['field_app_acronym'])) print $project_info['field_app_acronym']['und'][0]['value'];?></p>
<p><b>Consortium</b></p>
<table>
<tr>
  <th>No Acronym</th>
  <th>Partner</th>
  <th>Contact</th>
  <th>Country</th>
  <th>Total EUR</th>
  <th>Requested EUR</th>
</tr>
<?php
$index = 1;
foreach ($applications as $partner){
  $acronym = "";
  if(isset($partner['partner_info']['field_par_acronym']['und'][0]['value'])) $acronym = $partner['partner_info']['field_par_acronym']['und'][0]['value'];
  $my_org = "";
  if(isset($partner['user']['my_org'])) $my_org = $partner['user']['my_org'];
  $total = "";
  $requested = "";
  if(isset($partner['budget_table'][10])){
    $num_col = count($partner['budget_table'][10]);
    $total = $partner['budget_table'][10][$num_col-2];
    $requested = $partner['budget_table'][10][$num_col-1];
  }
  if($index % 2) print "<tr class='odd'>"; else print "<tr class='even'>";
  print "<td>" . $index . " " . $acronym . "</td>";
  print "<td>" . $my_org . "</td>";
  print "<td>" . $partner['user']['realname'] . "</td>";
  print "<td>" . $partner['user']['country'] . "</td>";
  print "<td>" . $total . "</td>";
  print "<td>" . $requested . "</td>";
  print "</tr>";
  $index++;
}
?>
</table>

<?php
if(isset($project_info)){
  foreach ($project_info['fields'] as $field){
    $field_name = "field_app_" . $field;
    if(array_key_exists($field_name, $project_info) && $project_info[$field_name]){
      print "<b>" . $project_info['labels'][$field_name]['title'] . "</b>";
      if($field_name == "field_app_topics" || $field_name == "field_app_keywords"){
        print "<ul>";
        foreach ($project_info[$field_name]['und'] as $row){
          print "<li>" . $row['value'] . "</li>";
        }
        print "</ul>";
      }
      else{
        print "<p>" . nl2br($project_info[$field_name]['und'][0]['value']) . "</p>";
      }
    }
  }
}
?>

<?php
$set_total_budget_msg = FALSE;
if(!isset($total_budget)) $set_total_budget_msg = TRUE;
else{
  print "<p><b>Total budget</b></p>";
  mkb_application_print_budget_table($total_budget);
}
?>
<h2>Partner information</h2>
<?php
$index = 1;
foreach ($applications as $partner){
  if(isset($partner['partner_info'])){
    $acronym = "";
    if(isset($partner['partner_info']['field_par_acronym']['und'][0]['value'])) $acronym = $partner['partner_info']['field_par_acronym']['und'][0]['value'];
    $title = "";
    if(isset($partner['partner_info']['title'])) $title = $partner['partner_info']['title'];
    $my_org = "";
    if(isset($partner['user']['my_org'])) $my_org = $partner['user']['my_org'];

    print "<p><b>" . $index . " " . $acronym  . " " . $partner['user']['country'] . "</b></p>";
    print "<p><b>" . $partner['user']['realname'] . "</b>, " . $my_org . "</p>";

    foreach ($partner_info['fields'] as $field){
      $field_name = "field_par_" . $field;
      if(array_key_exists($field_name, $partner['partner_info']) && $partner['partner_info'][$field_name]){
        print "<b>" . $partner_info['labels'][$field_name]['title'] . "</b>";
        print "<p>" . nl2br($partner['partner_info'][$field_name]['und'][0]['value']) . "</p>";
      }
    }
  }
  else{
    drupal_set_message($partner['user']['realname'] . t(' has not created a partner info. Please create one to see partner information and partner budget.'), $type = 'warning');
  }
  if(isset($partner['budget_table']) && isset($partner['partner_info'])){
    print "<b>Budget</b>";
    mkb_application_print_budget_table($partner['budget_table']);
	$field_name = 'field_bud_budget_info';
    if(array_key_exists($field_name, $partner['partner_budget']) && $partner['partner_budget'][$field_name]){
      print "<b>" . t('Supplementary budget information') . "</b>";
      print "<p>" . nl2br($partner['partner_budget'][$field_name]['und'][0]['value']) . "</p>";
    }
  }
  elseif(!isset($partner['budget_table'])){
    drupal_set_message($partner['user']['realname'] . t(' has not created a partner budget.'), $type = 'warning');
  }
  // index is used for numbering the partners
  $index++;
}
if($set_total_budget_msg) drupal_set_message(t('The total budget table will be generated when all partners have made a budget.'), $type = 'warning');
?>
</div>
