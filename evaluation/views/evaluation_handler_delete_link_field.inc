<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class evaluation_handler_delete_link_field extends evaluation_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};
    
    //Creating a dummy evaluation to check access against
    $dummy_evaluation = (object) array('type' => $type);
    if (!evaluation_access('edit', $dummy_evaluation)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $evaluation_id = $values->{$this->aliases['evaluation_id']};
    
    return l($text, 'admin/content/evaluations/evaluation/' . $evaluation_id . '/delete');
  }
}
