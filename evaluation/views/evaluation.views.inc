<?php

/**
 * @file
 * Providing extra functionality for the Evaluation UI via views.
 */


/**
 * Implements hook_views_data()
 */
function evaluation_views_data_alter(&$data) { 
  $data['evaluation']['link_evaluation'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the evaluation.'),
      'handler' => 'evaluation_handler_link_field',
    ),
  );
  $data['evaluation']['edit_evaluation'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the evaluation.'),
      'handler' => 'evaluation_handler_edit_link_field',
    ),
  );
  $data['evaluation']['delete_evaluation'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the evaluation.'),
      'handler' => 'evaluation_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows evaluations/evaluation/%evaluation_id/op
  $data['evaluation']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this evaluation.'),
      'handler' => 'evaluation_handler_evaluation_operations_field',
    ),
  );
}


/**
 * Implements hook_views_default_views().
 */
function evaluation_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'evaluations';
  $view->description = 'A list of all evaluations';
  $view->tag = 'evaluations';
  $view->base_table = 'evaluation';
  $view->human_name = 'Evaluations';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Evaluations';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any evaluation type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'evaluation_id' => 'evaluation_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'evaluation_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No evaluations have been created yet';
  /* Field: Evaluation: Evaluation ID */
  $handler->display->display_options['fields']['evaluation_id']['id'] = 'evaluation_id';
  $handler->display->display_options['fields']['evaluation_id']['table'] = 'evaluation';
  $handler->display->display_options['fields']['evaluation_id']['field'] = 'evaluation_id';
  $handler->display->display_options['fields']['evaluation_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['evaluation_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['evaluation_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['evaluation_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['evaluation_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['evaluation_id']['empty_zero'] = 0;
  /* Field: Evaluation: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'evaluation';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  /* Field: Evaluation: Link */
  $handler->display->display_options['fields']['link_evaluation']['id'] = 'link_evaluation';
  $handler->display->display_options['fields']['link_evaluation']['table'] = 'evaluation';
  $handler->display->display_options['fields']['link_evaluation']['field'] = 'link_evaluation';
  $handler->display->display_options['fields']['link_evaluation']['label'] = 'View';
  $handler->display->display_options['fields']['link_evaluation']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['external'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['link_evaluation']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['link_evaluation']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['alter']['html'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['link_evaluation']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['link_evaluation']['hide_empty'] = 0;
  $handler->display->display_options['fields']['link_evaluation']['empty_zero'] = 0;
  /* Field: Evaluation: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'evaluation';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'evaluations_admin_page');
  $handler->display->display_options['path'] = 'admin/content/evaluations/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Evaluations';
  $handler->display->display_options['tab_options']['description'] = 'Manage evaluations';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['evaluations'] = array(
    t('Master'),
    t('Evaluations'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Empty '),
    t('No evaluations have been created yet'),
    t('Evaluation ID'),
    t('.'),
    t(','),
    t('Name'),
    t('View'),
    t('Operations links'),
    t('Page'),
  );
  $views[] = $view;
  return $views;

}
