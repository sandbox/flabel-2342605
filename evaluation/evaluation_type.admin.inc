<?php

/**
 * @file
 * Evaluation type editing UI.
 */

/**
 * UI controller.
 */
class EvaluationTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage evaluation entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the evaluation type editing form.
 */
function evaluation_type_form($form, &$form_state, $evaluation_type, $op = 'edit') {

  if ($op == 'clone') {
    $evaluation_type->label .= ' (cloned)';
    $evaluation_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $evaluation_type->label,
    '#description' => t('The human-readable name of this evaluation type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($evaluation_type->type) ? $evaluation_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $evaluation_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'evaluation_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this evaluation type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['sample_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('An interesting evaluation switch'),
    '#default_value' => !empty($evaluation_type->data['sample_data']),
  );
//   $form['data']['eval_conf_type'] = array(
//       '#type' => 'select',
//       '#title' => t('Evaluation configuration type'),
//       '#default_value' => !empty($evaluation_type->data['eval_conf_type']),
//   );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save evaluation type'),
    '#weight' => 40,
  );

  //Locking not supported yet
  /*if (!$evaluation_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete evaluation type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('evaluation_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function evaluation_type_form_submit(&$form, &$form_state) {
  $evaluation_type = entity_ui_form_submit_build_entity($form, $form_state);
  $evaluation_type->save();
  $form_state['redirect'] = 'admin/structure/evaluation_types';
}

/**
 * Form API submit callback for the delete button.
 */
function evaluation_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/evaluation_types/manage/' . $form_state['evaluation_type']->type . '/delete';
}
