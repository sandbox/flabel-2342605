<?php

/**
 * @file
 * Evaluation editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class EvaluationUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Evaluations',
      'description' => 'Add edit and update evaluations.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of evaluations.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a evaluation',
      'description' => 'Add a new evaluation',
      'page callback'  => 'evaluation_add_page',
      'access callback'  => 'evaluation_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'evaluation.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );

    // Add menu items to add each different type of entity.
    foreach (evaluation_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type . '/%' . '/%'] = array(
        'title' => '',
        'page callback' => 'evaluation_form_wrapper',
        'page arguments' => array(array('type' => $type->type), $id_count +2, $id_count +3),
        'access callback' => 'evaluation_access',
        'access arguments' => array('edit', $type->type),
        'file' => 'evaluation.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing evaluation entities
    $items[$this->path . '/evaluation/' . $wildcard] = array(
      'page callback' => 'evaluation_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'evaluation_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'evaluation.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/evaluation/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/evaluation/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'evaluation_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'evaluation_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'evaluation.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Menu item for viewing evaluations
    $items['evaluation/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'evaluation_page_title',
      'title arguments' => array(1),
      'page callback' => 'evaluation_page_view',
      'page arguments' => array(1),
      'access callback' => 'evaluation_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }


  /**
   * Create the markup for the add Evaluation Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('evaluation_add_list', array('content' => $content));
  }

}


/**
 * Form callback wrapper: create or edit a evaluation.
 *
 * @param $evaluation
 *   The evaluation object being edited by this form.
 *
 * @see evaluation_edit_form()
 */
function evaluation_form_wrapper($evaluation, $eid = NULL, $gid = NULL) {
  // Add the breadcrumb for the form's location.
  evaluation_set_breadcrumb();

  if(isset($eid) && isset($gid)){
    $type = $evaluation['type'];
    $evaluation = evaluation_create(array('type' => $type, 'eid' => $eid, 'gid' => $gid));
  }
  return drupal_get_form('evaluation_edit_form', $evaluation);
}


/**
 * Form callback wrapper: delete a evaluation.
 *
 * @param $evaluation
 *   The evaluation object being edited by this form.
 *
 * @see evaluation_edit_form()
 */
function evaluation_delete_form_wrapper($evaluation) {
  // Add the breadcrumb for the form's location.
  //evaluation_set_breadcrumb();
  return drupal_get_form('evaluation_delete_form', $evaluation);
}


/**
 * Form callback: create or edit a evaluation.
 *
 * @param $evaluation
 *   The evaluation object to edit or for a create form an empty evaluation object
 *     with only a evaluation type defined.
 */
function evaluation_edit_form($form, &$form_state, $evaluation) {
  $eval_conf = $evaluation->eid();
  $group = $evaluation->gid();

  // Use evaluation configuration node type (eg. formality_evaluation) to setup the configuration
  // specific elements of the form.
  $type = "none";
  switch ($eval_conf->type) {
      case "form_eval":
          $type = 'b';
          break;
      case "eligible_evaluation":
          $type = 'b';
          break;
      case "quality_evaluation":
          $type = 'a';
          break;
      case "scientific_evaluation":
          $type = 'c';
          break;
      case "sector_evaluation":
          $type = 'd';
          break;
      case "rapporteur_evaluation":
          $type = 'e';
          break;

//       case "expert_evaluation":
//           $type = 'c';
//           break;
  }

  $type_options = array();
  foreach ($eval_conf->{'field_eval_o_' . $type}['und'] as $option){
    $type_options[] = $option['value'];
  }
  $type_question = array();
  foreach ($eval_conf->{'field_eval_q_' . $type}['und'] as $question){
    $type_question[] = $question['value'];
  }

  $form['Title'] = array(
    '#markup' => '<h2>' . $eval_conf->title . '</h2>',
    '#weight' => 0,
  );

  // Add the default field elements.
  $evaluation_name = $GLOBALS['user']->uid . '_' . $evaluation->gid . '_' . $evaluation->eid;
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => isset($evaluation->name) ? $evaluation->name : $evaluation_name,
  );

  $form['eid'] = array(
      '#type' => 'hidden',
      '#value' => isset($evaluation->eid) ? $evaluation->eid : '',
    );
  $form['gid'] = array(
      '#type' => 'hidden',
      '#value' => isset($evaluation->gid) ? $evaluation->gid : '',
    );

  $form['recommendation_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
        'class' => array('container-inline'),
        ),
    '#weight' => 6,
  );

  $form['recommendation_container']['recommendation'] = array(
    '#type' => 'select',
    '#title' => t('Recommendation'),
    '#options' => $evaluation->getRecommendationOptions(),
    '#default_value' => isset($evaluation->recommendation) ? $evaluation->recommendation() : '',
//     '#description' => '',
  );

  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
    '#default_value' => isset($evaluation->comment) ? $evaluation->comment : '',
    '#weight' => 7,
  );

//   $form['data']['#tree'] = TRUE;
//   $form['data']['sample_data'] = array(
//     '#type' => 'checkbox',
//     '#title' => t('An interesting evaluation switch'),
//     '#default_value' => isset($evaluation->data['sample_data']) ? $evaluation->data['sample_data'] : 1,
//     '#weight' => 2,
//   );

  // Add the configuration specific elements.
  if($type == 'a' || $type == 'b' || $type == 'c'){
    $form['eval_type'] = array(
        '#tree' => TRUE,
        '#type' => 'container',
        '#prefix' => '<table class="eval_type_tbl"><th colspan="2">' . t('Header for type specific questions') . '</th>',
        '#suffix' => '</table>',
        '#weight' => 3,
        );

    $form['eval_type']['answer_field_name'] = array(
        '#type' => 'hidden',
        '#value' => 'field_answer_' . $type,
        );

    $form['eval_type']['data'] = array();
    for($i = 0; $i < count($type_question); $i++){
      $form['eval_type']['data'][$i]['question'] = array(
          '#markup' => $type_question[$i],
          '#prefix' => '<tr><td class="tbl_question">',
          '#suffix' => '</td>',
          );
      $form['eval_type']['data'][$i]['options'] = array(
          '#type' => 'select',
          '#options' => $type_options,
          '#default_value' => isset($evaluation->{'field_answer_' . $type}['und'][$i]['value']) ? $evaluation->{'field_answer_' . $type}['und'][$i]['value'] : '',
          '#prefix' => '<td class="tbl_options">',
          '#suffix' => '</td></tr>',
          );
    }
  }
  // Rapporteur evaluation
  if($type == 'e'){
    // Hide fields
    $form['recommendation_container']['#access'] = FALSE;
    $form['comment']['#access'] = FALSE;

    // Build content from evaluations
    // Scientific evaluations
    $eval_ids = evaluation_get_evaluations_by_gid($evaluation->gid, array('evaluation_c'));
    $content = array();
    $scores = array();
    foreach ($eval_ids as $eval_index => $eval_id){
      $eval = evaluation_load($eval_id->evaluation_id);
      $eval_conf = node_load($eval->eid);
      $type_letter = substr($eval->type, -1);

      // Title = Evaluation type
      $content[$eval_conf->type]['title'] = $eval_conf->title;

      // Data from evaluation
      $eval_wrapper = entity_metadata_wrapper('evaluation', $eval);
      $content[$eval_conf->type]['data'][$eval->evaluation_id]['header'] = array(0 => 'Question', 1 => 'Answer');

      foreach ($eval->{'field_answer_' . $type_letter}['und'] as $answer_index => $answer){
        $content[$eval_conf->type]['data'][$eval->evaluation_id]['rows'][$answer_index]['question'] = $eval_conf->{'field_eval_q_' . $type_letter}['und'][$answer_index]['value'];
        $answer_value = $answer['value'];
        $content[$eval_conf->type]['data'][$eval->evaluation_id]['rows'][$answer_index]['answer'] = $eval_conf->{'field_eval_o_' . $type_letter}['und'][$answer_value]['value'];
      }

      $content[$eval_conf->type]['data'][$eval->evaluation_id]['comment'] = isset($eval->comment) ? $eval->comment : '';
      $author = user_load($eval->uid);
      $content[$eval_conf->type]['data'][$eval->evaluation_id]['author'] = $author->realname;
      $score = $eval_wrapper->field_score->value();
      $content[$eval_conf->type]['data'][$eval->evaluation_id]['score'] = isset($score) ? $score : '0.0';
      $condition = $eval_wrapper->field_condition->value();
      if($condition == 0) $condition = 'No';
      elseif($condition == 1) $condition = 'Yes';
      $content[$eval_conf->type]['data'][$eval->evaluation_id]['condition'] = isset($condition) ? $condition : '';
      $content[$eval_conf->type]['data'][$eval->evaluation_id]['recommendation'] = isset($eval->recommendation) ? $eval->recommendation() : '';

      // Add score to scores for score summary
      $scores[] = isset($score) ? $score : 0;
    }

    $form['scientific'] = array(
       '#type' => 'fieldset',
       '#title' => 'Scientific evaluations',
       '#weight' => -10,
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
       );

    foreach ($content as $eval_type){
      // Questions and answers
      $i = 0;
      foreach ($eval_type['data'] as $eval){
      $table = array(
          'header' => $eval['header'],
          'rows' => $eval['rows'],
          'attributes' => array(
              'class' => array('table_class'),
              'width' => '50%',
              ),
          'sticky' => FALSE,
          'empty' => 'No evaluations of this type.',
          'colgroups' => array(),
          'caption' => $eval['author'],
          );

      $form['scientific'][$i]['table'] = array(
          '#markup' =>  theme_table($table),
          '#weight' => 1,
          );

      $form['scientific'][$i]['condition'] = array(
          '#markup' =>  '<p>Did the researchers meet the conditions of the invitation letter? ' . $eval['condition'] . '</p>',
          '#weight' => 2,
          );

      $form['scientific'][$i]['score'] = array(
          '#markup' =>  '<p>Score: ' . $eval['score'] . '</p>',
          '#weight' => 3,
          );

      $form['scientific'][$i]['recommendation'] = array(
          '#markup' =>  '<p>Recommendation: <b>' . $eval['recommendation'] . '</b></p>',
          '#weight' => 4,
          );

      $form['scientific'][$i]['comment'] = array(
          '#markup' =>  '<p>Comment: ' . $eval['comment'] . '</p></br>',
          '#weight' => 5,
          );
      $i++;
      }
    }

    // Sector evaluations
    $form['sector'] = array(
       '#type' => 'fieldset',
       '#title' => 'Sector evaluations',
       '#weight' => -9,
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
       );

    $eval_ids = evaluation_get_evaluations_by_gid($evaluation->gid, array('evaluation_d'));
    $ids = array();
    foreach ($eval_ids as $eval_id){
      $ids[] = $eval_id->evaluation_id;
    }

    $evals = evaluation_load_multiple($ids);
    $i = 2;
    foreach ($evals as $eval){
      $wrapper = entity_metadata_wrapper('evaluation', $eval);
      // Container
      $form['sector'][$i] = array(
          '#type' => 'container',
          '#weight' => $i,
          );

      // User
      $account = user_load($wrapper->uid->value());
      $form['sector'][$i]['user'] = array(
          '#markup' => '<p><b>' . $account->realname . '</b></p>',
          '#weight' => 0,
          );

      // Relevans
      $relevans = $wrapper->field_se_relevans->value();
      $form['sector'][$i]['evaluations'] = array(
          '#markup' => '<p>Relevans: ' . $relevans . '</p></br>',
          '#weight' => 1,
          );
      $i++;
    }

    // Add relevans field
    $field = field_info_field('field_ra_rapport');
    $instance = field_info_instance('evaluation', 'field_ra_rapport', 'evaluation_e');
    $my_field = field_default_form('evaluation', NULL, $field, $instance, LANGUAGE_NONE, array(), $form, $form_state);
    $form += (array) $my_field;

    // Set default value in form
    $wrapper = entity_metadata_wrapper('evaluation', $evaluation);
    $rapport = $wrapper->field_ra_rapport->value();
    $form['field_ra_rapport']['und'][0]['value']['#default_value'] = isset($rapport) ? $rapport : '';

    // Score summary
    $score_sum = 0;
    foreach ($scores as $score){
      $score_sum += $score;
    }
    $score_average = $score_sum/count($scores);


    $form['score_average'] = array(
        '#markup' => '<p>Average score: ' . $score_average . '</p>',
        '#weight' => 1,
        );

    $field = field_info_field('field_score');
    $instance = field_info_instance('evaluation', 'field_score', 'evaluation_e');
    $my_field = field_default_form('evaluation', NULL, $field, $instance, LANGUAGE_NONE, array(), $form, $form_state);
    $form += (array) $my_field;
    $score_default = $wrapper->field_score->value();
    if(isset($score_default)){
      $form['field_score']['und'][0]['value']['#default_value'] = $score_default;
    }
    else{
      $form['field_score']['und'][0]['value']['#default_value'] = isset($score_average) ? $score_average : '0.0';
    }
  }


  if($type == 'd'){
    // Hide fields
    $form['recommendation_container']['#access'] = FALSE;
    $form['comment']['#access'] = FALSE;

    // Add relevans field
    $field = field_info_field('field_se_relevans');
    $instance = field_info_instance('evaluation', 'field_se_relevans', 'evaluation_d');
    $my_field = field_default_form('evaluation', NULL, $field, $instance, LANGUAGE_NONE, array(), $form, $form_state);
    $form += (array) $my_field;

    // Set default value in form
    $wrapper = entity_metadata_wrapper('evaluation', $evaluation);
    $relevans = $wrapper->field_se_relevans->value();
    $form['field_se_relevans']['und'][0]['value']['#default_value'] = isset($relevans) ? $relevans : '';
  }

  //
  if($type == 'c'){
    // Add condition field
    $field = field_info_field('field_condition');
    $instance = field_info_instance('evaluation', 'field_condition', 'evaluation_c');
    $my_field = field_default_form('evaluation', NULL, $field, $instance, LANGUAGE_NONE, array(), $form, $form_state);
    $form += (array) $my_field;
    $form['field_condition']['#weight'] = 5;

    // Set default value in form
    $wrapper = entity_metadata_wrapper('evaluation', $evaluation);
    $condition = $wrapper->field_condition->value();
    $form['field_condition']['und']['#default_value'] = isset($condition) ? $condition : 0;
  }

  // Type a is the only one so far to have the score field
  if($type == 'a' || $type == 'c'){
    // Create the meter with an image and the label from evaluation config node options a and c
    $full_image = '';
    $labels = '';
    for($i = 0; $i < count($type_options); $i++){
      $image_file_name = 'meter.png';
      if($i==0){
        $image_file_name = 'meter_start.png';
      }
      if($i==count($type_options)-1){
        $image_file_name = 'meter_end.png';
      }

      $image_options = array(
        'path' => 'sites/all/modules/evaluation/img/' . $image_file_name,
        'alt' => 'Test alt',
        'title' => 'Test title',
        'width' => '100px',
        'height' => '20px',
        'attributes' => array('class' => '', 'id' => '', 'style' => 'position: relative; top: 0; left:' . $i*100+50 . ';',),
      );
      $full_image .= theme('image', $image_options);
      $labels .= '<div class="pointer_label">' . $type_options[$i] . '</div>';
    }

    // Create the image of the pointer and add calculate functionality
    if(isset($evaluation->field_score['und'][0]['value'])){
      $pointer_pos = (100*$evaluation->field_score['und'][0]['value'])+50;
      $image_options_pointer = array(
        'path' => 'sites/all/modules/evaluation/img/pointer.png',
        'alt' => 'Test alt',
        'title' => 'Test title',
        'width' => '2px',
        'height' => '15px',
        'attributes' => array('class' => 'pointer', 'id' => 'pointer', 'style' => 'position: absolute; top: 23px; left: ' . $pointer_pos . 'px;',),
      );
      $full_image .= theme('image', $image_options_pointer);
    }

    $form['score'] = array(
    '#markup' => '',
    '#prefix' => '<table class="score_tbl">',
    '#suffix' => '</table>',
    '#weight' => 4,
    );

    // Add js
    $form['#attached']['js'] = array(
    drupal_get_path('module', 'evaluation') . '/js/score.js');

    // Prevent reload
    $form['#attributes'] = array();

    // Add Recalculate score button
    $form['score']['calc'] = array(
      '#type' => 'submit',
      '#value' => t('Calculate score'),
      '#attributes' => array(
          'onclick' => 'calcScore(this);',
          'onsubmit' => 'return false',
          ),
      '#executes_submit_callback' => FALSE,
      '#prefix' => '<tr><td class="score_btn">',
      '#suffix' => '</td>',
    );

    $form['score']['meter'] = array(
      '#markup' => '<div class="meter_wrapper"><div class="pointer_labels">'. $labels . '</div>' .
        '<div class="pointer_meter">' . $full_image .'</div></div>',
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    );
  }

  // Add the field related form elements.
   $form_state['evaluation'] = $evaluation;
//    field_attach_form('evaluation', $evaluation, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save evaluation'),
    '#submit' => $submit + array('evaluation_edit_form_submit'),
  );

  if (!empty($evaluation->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete evaluation'),
      '#suffix' => l(t('Cancel'), 'admin/content/evaluations'),
      '#submit' => $submit + array('evaluation_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'evaluation_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the evaluation form
 */
function evaluation_edit_form_validate(&$form, &$form_state) {
  if(isset($form['eval_type'])){
    // Get the field name from the evaluation type
    $answer_field_name = $form['eval_type']['answer_field_name']['#value'];

    // Get the answers from the configuration specific elements
    for($i = 0; $i < count($form_state['values']['eval_type']['data']); $i++){
       $answers['und'][$i]['value'] = $form_state['values']['eval_type']['data'][$i]['options'];
    }
    $form_state['evaluation']->$answer_field_name = $answers;
    // Remove configuration specific elements from form_state
    unset($form_state['values']['eval_type']);

    // If evaluation is type a: Calculate score and add it to form_state
    if($answer_field_name == 'field_answer_a' || $answer_field_name == 'field_answer_c'){
      $score_sum = 0;
      for($i = 0; $i < count($answers['und']); $i++){
        $score_sum += $answers['und'][$i]['value'];
      }
      $score['und'][0]['value'] = $score_sum/count($answers['und']);
      $form_state['evaluation']->field_score = $score;
    }
  }

  $evaluation = $form_state['evaluation'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('evaluation', $evaluation, $form, $form_state);
}


/**
 * Form API submit callback for the evaluation form.
 *
 * @todo remove hard-coded link
 */
function evaluation_edit_form_submit(&$form, &$form_state) {
  $evaluation = entity_ui_controller('evaluation')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the evaluation and go back to the list of evaluations

  // Add in created and changed times.
  if ($evaluation->is_new = isset($evaluation->is_new) ? $evaluation->is_new : 0){
    $evaluation->created = time();
  }

  $evaluation->changed = time();

  $evaluation->save();

  // Set destination.
  $eid = isset($_SESSION['mkb_eval']['eid']) ? '/eid=' . $_SESSION['mkb_eval']['eid'] : '';
  $fid = isset($_SESSION['mkb_eval']['fid']) ? '/fid=' . $_SESSION['mkb_eval']['fid'] : '';
  if(isset($_SESSION['mkb_eval'])){
    $form_state['redirect'] = $GLOBALS['base_url'] . '/eval/' . $_SESSION['mkb_eval']['cid'] .
    '/' . $_SESSION['mkb_eval']['vid'] . $eid . $fid;
  }
  else{
    $form_state['redirect'] = 'admin/content/evaluations';
  }

}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function evaluation_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/evaluations/evaluation/' . $form_state['evaluation']->evaluation_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a evaluation.
 *
 * @param $evaluation
 *   The evaluation to delete
 *
 * @see confirm_form()
 */
function evaluation_delete_form($form, &$form_state, $evaluation) {
  $form_state['evaluation'] = $evaluation;

  $form['#submit'][] = 'evaluation_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete evaluation %name?', array('%name' => $evaluation->name)),
    'admin/content/evaluations/evaluation',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for evaluation_delete_form
 */
function evaluation_delete_form_submit($form, &$form_state) {
  $evaluation = $form_state['evaluation'];

  evaluation_delete($evaluation);

  drupal_set_message(t('The evaluation %name has been deleted.', array('%name' => $evaluation->name)));
  watchdog('evaluation', 'Deleted evaluation %name.', array('%name' => $evaluation->name));

  $form_state['redirect'] = 'admin/content/evaluations';
}



/**
 * Page to add Evaluation Entities.
 *
 * @todo Pass this through a proper theme function
 */
function evaluation_add_page() {
  $controller = entity_ui_controller('evaluation');
  return $controller->addPage();
}


/**
 * Displays the list of available evaluation types for evaluation creation.
 *
 * @ingroup themeable
 */
function theme_evaluation_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="evaluation-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer evaluation types')) {
      $output = '<p>' . t('Evaluation Entities cannot be added because you have not created any evaluation types yet. Go to the <a href="@create-evaluation-type">evaluation type creation page</a> to add a new evaluation type.', array('@create-evaluation-type' => url('admin/structure/evaluation_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No evaluation types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}





/**
 * Sets the breadcrumb for administrative evaluation pages.
 */
function evaluation_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Evaluations'), 'admin/content/evaluations'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

/**
 * @param $gid
 *   Group id.
 * @param $type
 *   Evaluation type.
 * @return $evaluation_ids
 *   Evaluation ids belonging to this group.
 */
function evaluation_get_evaluations_by_gid($gid, $types) {
  $query = db_select("evaluation", "e");
  $query ->addField("e", "evaluation_id", "evaluation_id");
  $query
  ->condition("e.gid", $gid, "=")
  ->condition("e.type", $types, "IN");;
  $result = $query->execute()->fetchAll();
  return $result;
}

