<?php

/**
 * @file
 * Example tpl file for theming a single evaluation-specific theme
 *
 * Available variables:
 * - $status: The variable to theme (while only show if you tick status)
 * 
 * Helper variables:
 * - $evaluation: The Evaluation object this status is derived from
 */
?>

<div class="evaluation-status">
  <?php print '<strong>Evaluation Sample Data:</strong> ' . $evaluation_sample_data = ($evaluation_sample_data) ? 'Switch On' : 'Switch Off' ?>
</div>