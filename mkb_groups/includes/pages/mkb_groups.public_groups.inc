<?php
module_load_include('inc', 'mkb_groups', 'includes/mkb_groups.helper');

/**
 * @return $template
 *   Rendered public_groups_template
 */
function mkb_groups_public_groups_function(){
  $variables = array();
  $account = $GLOBALS['user'];

  // Get all groups
  $gids = og_get_all_group('node');
  $groups = node_load_multiple($gids);

  // Public groups
  $public_groups = mkb_groups_get_groups_by_visibility($groups, 0);
  $public_header = array('Public groups', 'Subscribe');
  $public_rows = array();

  foreach($public_groups as $gid => $group){
    $author = user_load($group->uid);
    $wrapper = entity_metadata_wrapper('node', $group);

    // Create subscribe link if current user is not member of group
    $subscribe = og_is_member('node', $gid, 'user', $account, array(OG_STATE_ACTIVE)) ?
      l('Workspace', 'groups/workspace/members/' . $gid) :
      l('Join group', 'group/node/' . $gid . '/subscribe/og_user_node');

    $public_rows[$gid] = array(
        'group' => '<b>' . $wrapper->title->value() . '</b></br>' .
        $wrapper->body->value->value(array('decode' => FALSE)) . '</br>' .
        'Contact: ' . l($author->realname, 'user-profile/' . $group->uid),
        'subscribe' => $subscribe,
        );
  }

  $variables['groups'] = array(
      'public' => array('header' => $public_header, 'rows' => $public_rows),
      );
  return theme('public_groups_template', $variables);
}