<?php
module_load_include('inc', 'mkb_groups', 'includes/mkb_groups.helper');

/**
 * @return $template
 *   Rendered my_groups_template
 */
function mkb_groups_my_groups_function(){
  $variables = array();
  $account = $GLOBALS['user'];
  $gids = og_get_groups_by_user($account, 'node');
  $groups = node_load_multiple($gids);

  // Private groups
  $private_groups = mkb_groups_get_groups_by_visibility($groups, 1);
  $private_header = array('Private groups', 'Workspace');
  $private_rows = array();

  foreach($private_groups as $gid => $group){
    $author = user_load($group->uid);
    $wrapper = entity_metadata_wrapper('node', $group);
    $private_rows[$gid] = array(
        'group' => '<b>' . $wrapper->title->value() . '</b></br>' .
        $wrapper->body->value->value(array('decode' => FALSE)) . '</br>' .
        'Contact: ' . l($author->realname, 'user-profile/' . $group->uid),
        'workspace' => l('Workspace', 'groups/workspace/members/' . $gid),
        );
  }

  // Public groups
  $public_groups = mkb_groups_get_groups_by_visibility($groups, 0);
  $public_header = array('Public groups', 'Workspace');
  $public_rows = array();

  foreach($public_groups as $gid => $group){
    $author = user_load($group->uid);
    $wrapper = entity_metadata_wrapper('node', $group);
    $public_rows[$gid] = array(
        'group' => '<b>' . $wrapper->title->value() . '</b></br>' .
        $wrapper->body->value->value(array('decode' => FALSE)) . '</br>' .
        'Contact: ' . l($author->realname, 'user-profile/' . $group->uid),
        'workspace' => l('Workspace', 'groups/workspace/members/' . $gid),
        );
  }

  $variables['groups'] = array(
      'private' => array('header' => $private_header, 'rows' => $private_rows),
      'public' => array('header' => $public_header, 'rows' => $public_rows),
      );
  return theme('my_groups_template', $variables);
}