<div class="groups_table">
<h2 class="block-title">Public groups</h2>
</br>

<?php
// Public groups
$public_header = array();
$public_header[0] = array(
    'data' => $variables['groups']['public']['header'][0],
    'class' => 'groups_first_row',
    );
$public_header[1] = array(
    'data' => $variables['groups']['public']['header'][1],
    'class' => 'groups_second_row',
    );

$table = array(
    'header' => $public_header,
    'rows' => $variables['groups']['public']['rows'],
    'attributes' => array(
        'class' => array('table_class'),
        'width' => '100%',
        ),
    'sticky' => FALSE,
    'empty' => 'There are no public groups.',
    'colgroups' => array(),
    'caption' => '',
    );
print theme_table($table);
?>
</div>

