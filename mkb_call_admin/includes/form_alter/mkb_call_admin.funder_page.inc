<?php
/**
 * @file
 * Code for the mkb_call_admin.funder_page.
 */
module_load_include('inc', 'mkb_call_admin', 'includes/mkb_call_admin.helper');

function mkb_call_admin_fp_callback($form, $form_state) {
  return render($form['funding']);
}

function mkb_call_admin_funder_page($form, $form_state, $form_id){
   //Hide title
  $form['title']['#access'] = FALSE;

  //FUNDING WRAPPER
  $form['funding'] = array(
    //'#weight' => '3',
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#prefix' => '<div id="funding_wrapper-div">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  //CALL IDENTIFIER - Selectbox
  $form['funding']["field_call_id"] = $form["field_call_id"];
  unset( $form["field_call_id"]);

  //CALL IDENTIFIER - Add ajax
  $form['funding']["field_call_id"]["und"]["#ajax"] = array(
    'callback' => 'mkb_call_admin_fp_callback',
    'wrapper' => 'funding_wrapper-div',
    'method' => 'replace',
    'effect' => 'fade',
  );

  //FUNDING AGENCY - MOVE FIELDS TO FUNDING
  $form['funding']['field_funding_agency'] = $form['field_funding_agency'];
  unset($form['field_funding_agency']);

  //FUNDING AGENCY - Add ajax
  $form['funding']["field_funding_agency"]["und"]["#ajax"] = array(
    'callback' => 'mkb_call_admin_fp_callback',
    'wrapper' => 'funding_wrapper-div',
    'method' => 'replace',
    'effect' => 'fade',
  );

  //FUNDING - MOVE FIELDS TO FUNDING
  $form['funding']['field_country_specific'] = $form['field_country_specific'];
  unset($form['field_country_specific']);

  $form['funding']['field_funding'] = $form['field_funding'];
  unset($form['field_funding']);

  $form['funding']['field_conditions_short'] = $form['field_conditions_short'];
  unset($form['field_conditions_short']);

  $form['funding']['field_conditions_full'] = $form['field_conditions_full'];
  unset($form['field_conditions_full']);

  $form['funding']['field_contact_persons'] = $form['field_contact_persons'];
  unset($form['field_contact_persons']);

  $form['funding']['field_call_managers'] = $form['field_call_managers'];
  unset($form['field_call_managers']);

  $form['funding']['field_evaluators'] = $form['field_evaluators'];
  unset($form['field_evaluators']);

  //CALL_ID - Set Visibility
  //CALL_ID - Get selected call_id
  $fp_visible = TRUE;
  $call_id = 0;
  $default = FALSE;
  if(isset($_POST["field_call_id"]["und"])){
    if($_POST["field_call_id"]["und"] == $form['funding']["field_call_id"]["und"]["#default_value"][0]
        || (isset($_SESSION["call_id_default"]) && $_SESSION["call_id_default"] == $_POST["field_call_id"]["und"])){
      //dpm("nothing has changed");
      $call_id = $_POST["field_call_id"]["und"];
    }
    else{
      //dpm("new funding agency selected");
      $call_id = $_POST["field_call_id"]["und"];
    }
    $_SESSION["call_id_default"] = $_POST["field_call_id"]["und"];
  }
  else{
    if(isset($form['funding']["field_call_id"]["und"]["#default_value"][0])){
      //dpm("default value");
      $call_id = $form['funding']["field_call_id"]["und"]["#default_value"][0];
      $default = TRUE;
    }
    else{
      //dpm("no default values yet");
      $fp_visible = FALSE;
    }
    unset($_SESSION["call_id_default"]);
  }

  if($fp_visible){
    $call_node = node_load($call_id);
    $topics = mkb_call_admin_get_topics_from_call_id($call_id);
    $theme_funding = $call_node->field_theme_funding['und'][0]['value'];
    //Theme funding table
    if(count($topics)>0 && $theme_funding==1){
      //Theme funding description
      $tf_desc = $form['field_fp_theme_funding']['und']['#description'];

      //TOPICS_WRAPPER - Fieldset
      $form['funding']["topics_wrapper"] = array(
        '#tree' => FALSE,
        '#weight' => '5',
        '#type' => 'table',
        '#collapsible' => TRUE,
        '#prefix' => '<br/><label>Theme funding</label><div id="topic_table-div"><table class="table_trimmed_no_b"><tbody>',
        '#suffix' => '</tbody></table>' . $tf_desc . '</div><br/>',
      );

      $form['funding']["topics_wrapper"]['field_fp_theme_funding'] = $form['field_fp_theme_funding'];

      //TOPICS
      unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und']['#theme']);
      unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und']['#prefix']);
      unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und']['#suffix']);
      unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und']['add_more']);
      $form['funding']["topics_wrapper"]['field_fp_theme_funding']['und']['#max_delta'] = count($topics)-1;
      for($i=0; $i<count($topics); $i++){
        if(isset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][$i]['value']['#default_value'])){
          $default_value = $form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][$i]['value']['#default_value'];
          unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][$i]);
        }
        else{
          $default_value = 0;
        }

        $label = $topics[$i]->title;
        $weight = $topics[$i]->weight;

        $element = array(
          '#type' => 'textfield',
          '#delta' => $i,
          '#default_value' => $default_value,
          '#prefix' => '<tr><td><b>' . $label . '</b></td><td> 1000 €</td><td>',
          '#suffix' => '</td></tr>',
          '#size' => 14,
          '#maxlength' => 12,
          '#number_type' => 'integer',
          '#element_validate' => array('number_field_widget_validate'),
          '#field_parents' => array(),
          '#field_name' => 'field_fp_theme_funding',
          '#language' => 'und',
          '#weight' => $weight,
          //'#precision' => 10,
          //'#access' => $access,
        );
        $form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][$i] = array();
        $form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][$i]['value'] = $element;
      }
      //Remove the extra field drupal adds to the form
      if($default){
        unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']['und'][count($topics)]);
      }
    }
    else{
      unset($form['funding']["topics_wrapper"]['field_fp_theme_funding']);
    }

    //Set title to what is selected in funding_agency
    $fa_id = 0;
    if(isset($_POST["field_funding_agency"]["und"])){
      $fa_id = $_POST["field_funding_agency"]["und"];
    }
    else{
      $fa_id = $form['funding']["field_funding_agency"]['und']['#default_value'][0];
    }
    $node_funding_agency = node_load($fa_id);
    $form['title']['#default_value'] = $node_funding_agency->title;
  }

  unset($form['field_fp_theme_funding']);

  $form['funding']["field_funding_agency"]['und']['#access'] = $fp_visible;
  $form['funding']["field_country_specific"]['und']['#access'] = $fp_visible;
  $form['funding']["field_funding"]['und']['#access'] = $fp_visible;
  $form['funding']["field_conditions_short"]['und']['#access'] = $fp_visible;
  $form['funding']["field_conditions_full"]['und']['#access'] = $fp_visible;
  $form['funding']["field_contact_persons"]['und']['#access'] = $fp_visible;
  $form['funding']["field_call_managers"]['und']['#access'] = $fp_visible;
  $form['funding']["field_evaluators"]['und']['#access'] = $fp_visible;
  return $form;
}
