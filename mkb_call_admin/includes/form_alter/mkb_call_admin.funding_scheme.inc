<?php
/**
 * @file
 * Code for the mkb_call_admin.funding_scheme.
 */
module_load_include('inc', 'mkb_call_admin', 'includes/mkb_call_admin.helper');

function mkb_call_admin_fs_callback($form, $form_state) {
  return render($form['budget']);
}

function mkb_call_admin_funding_scheme($form, $form_state, $form_id){
/*
  if (isset($_SESSION["mkb_gid"])) {
      $gid = $_SESSION["mkb_gid"];

      //Set gid in group audience and remove group audience from form
      $form['og_group_ref']['und'][0]['default']['#default_value'][0] = $gid;
      $form['og_group_ref']['#access'] = FALSE;
*/

  //TITLE - description
  $form['title']['#description'] = "Enter a title that applicants can use to select the correct funding scheme";

  //FORM - Add js
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'mkb_call_admin') . '/js/categories.js');

  //BUDGET_WRAPPER
  $form['budget'] = array(
    //'#weight' => '3',
    '#type' => 'fieldset',
    '#prefix' => '<div id="budget_wrapper-div">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  //CALL IDENTIFIER - Selectbox
  $form['budget']["field_call_id"] = $form["field_call_id"];
  unset( $form["field_call_id"]);

  //CALL IDENTIFIER - Add ajax
  $form['budget']["field_call_id"]["und"]["#ajax"] = array(
    'callback' => 'mkb_call_admin_fs_callback',
    'wrapper' => 'budget_wrapper-div',
    'method' => 'replace',
    'effect' => 'fade',
  );

  //FUNDER PAGE - Selectbox
  $form['budget']["field_funder_page"] = $form["field_funder_page"];
  $form['budget']["field_funder_page"]["und"]["#options"] = array();
  unset($form["field_funder_page"]);

  //SCHEME CONDITION - Selectbox
  $form['budget']["field_fun_schc"] = $form["field_fun_schc"];
  unset( $form["field_fun_schc"]);


  //FUNDER PAGE - Setup options
  $fp_visible = TRUE;
  $table_visible = TRUE;
  $budget_categories = array();
  $budget_cat_descrip = array();
  if(isset($_POST["field_call_id"]["und"])){
    if($_POST["field_call_id"]["und"] == $form['budget']["field_call_id"]["und"]['#default_value'][0]){
      //dpm("FP - nothing has changed");
      $nid = $form['budget']["field_call_id"]["und"]['#default_value'][0];
      $form['budget']["field_funder_page"]["und"]["#options"] = mkb_call_admin_get_fa_options($nid);
      $budget_categories = mkb_call_admin_get_values($nid, 'budget_categories');
      $budget_cat_descrip = mkb_call_admin_get_values($nid, 'budget_cat_descrip');
    }
    elseif($_POST["field_call_id"]["und"] == "_none"){
      //dpm("FP - has default value but - Select - selected in funding scheme");
      $table_visible = false;
      $fp_visible = false;
    }
    else{
      //dpm("FP - new call main page selected");
      $nid = $_POST["field_call_id"]["und"];
      $form['budget']["field_funder_page"]["und"]["#options"] = mkb_call_admin_get_fa_options($nid);
      $budget_categories = mkb_call_admin_get_values($nid, 'budget_categories');
      $budget_cat_descrip = mkb_call_admin_get_values($nid, 'budget_cat_descrip');
    }
  }
  else{
    if(isset($form['budget']["field_call_id"]["und"]['#default_value'][0])){
      //dpm("FP - default value");
      $nid = $form['budget']["field_call_id"]["und"]['#default_value'][0];
      $form['budget']["field_funder_page"]["und"]["#options"] = mkb_call_admin_get_fa_options($nid);
      $budget_categories = mkb_call_admin_get_values($nid, 'budget_categories');
      $budget_cat_descrip = mkb_call_admin_get_values($nid, 'budget_cat_descrip');
    }
    else{
      //dpm("FP - no default values yet");
      $form['budget']["field_funder_page"]["und"]["#options"] = array("_none" => "- None -");
      $fp_visible = FALSE;
      $table_visible = FALSE;
    }
  }

  //FUNDER PAGE - Add ajax
  $form['budget']["field_funder_page"]["und"]["#ajax"] = array(
    'callback' => 'mkb_call_admin_fs_callback',
    'wrapper' => 'budget_wrapper-div',
    'method' => 'replace',
    'effect' => 'fade',
  );

  //FUNDER PAGE - Visibility
  $form['budget']["field_funder_page"]['und']['#access'] = TRUE;
  $form['budget']["field_fun_schc"]['und']['#access'] = TRUE;
  if(!$fp_visible){
    $form['budget']["field_funder_page"]['und']['#access'] = FALSE;
    $form['budget']["field_fun_schc"]['und']['#access'] = FALSE;
  }

  //TABLE - Visibility
  if(isset($_POST["field_funder_page"]["und"]) && $_POST["field_funder_page"]["und"] != "_none"){
    if($_POST["field_funder_page"]["und"] == $form['budget']["field_funder_page"]["und"]["#default_value"][0]){
      //dpm("TABLE - nothing has changed");
    }
    else{
      //dpm("TABLE - new funder page selected");
    }
  }
  elseif(!isset($_POST["field_funder_page"]["und"])){
    if(isset($form['budget']["field_funder_page"]["und"]["#default_value"][0])){
      //dpm("TABLE - default value");
    }
    else{
      //dpm("TABLE - no default values yet");
      $table_visible = false;
    }
  }
  elseif($_POST["field_funder_page"]["und"] == "_none"){
    //dpm("TABLE - has default value but - Select - selected in funder page");
    $table_visible = false;
  }

  //TABLE - Headers
  $table_header = "";
  if($table_visible){
    $table_header .= '<tr><th>Project activities</th>';
    foreach($budget_categories as $row) {
        $table_header .= '<th>' . $row->value . '</th>';
      }
    $table_header .= '</tr>';
    if($budget_cat_descrip){
      $table_header .= '<tr><td>Description</td>';
      foreach($budget_cat_descrip as $row) {
          $table_header .= '<td>' . wordwrap($row->value,40,'<br>') . '</td>';
        }
      $table_header .= '</tr>';
    }
  }

  //TABLE - Footer
  $table_footer = "";
  if($table_visible){
    $table_footer .= "Check the budgets items that can be funded by this scheme. Enter max funding in percent.";
  }

  //TABLE_WRAPPER - Fieldset
  $form['budget']["table_wrapper"] = array(
    '#tree' => FALSE,
    '#weight' => '10',
    '#type' => 'table',
    '#collapsible' => TRUE,
    '#prefix' => '<br/><div id="budget_table-div"><table><tbody>' . $table_header,
    '#suffix' => '</tbody></table>' . $table_footer . '</div><br/>',
  );

  //TABLE - Build table
  $form['budget']['table_wrapper']['table'] = array();
  if($table_visible){
    $budget_names = array("categories", "pm", "pc", "ts", "cs", "eq", "sc", "oc", "ic", "rm");
    for($i=0; $i<count($budget_names); $i++){
      $row_name = "field_fun_" . $budget_names[$i];
      $class_name = "field-fun-" . $budget_names[$i];
      $form['budget']['table_wrapper']['table'][$row_name] = $form[$row_name];
      $label = $form['budget']['table_wrapper']['table'][$row_name]['und']['#title'];
      $form['budget']['table_wrapper']['table'][$row_name]['und']['#cardinality'] = count($budget_categories);
      $form['budget']['table_wrapper']['table'][$row_name]['und']['#max_delta'] = $i;
      unset($form['budget']['table_wrapper']['table'][$row_name]['und']['#attributes']['class']);
      unset($form['budget']['table_wrapper']['table'][$row_name]['und']['#theme']);
      for($j=0; $j<count($budget_categories); $j++){
        $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#prefix'] = "<td>";
        $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#suffix'] = "</td>";

        if ($j == 0) $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#prefix'] = '<tr><td>' . $label . '</td><td>';
        if ($j == count($budget_categories)-1) $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#suffix'] = '</td></tr>';

        if($row_name == "field_fun_rm"){
          //Remove weight from MAX FUNDED
          unset($form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['_weight']);
        }
        elseif($row_name == "field_fun_categories"){
          //CATEGORIES - Add js
          $form['budget']['table_wrapper']['table']['field_fun_categories']['und'][$j]['value']['#attributes'] = array(
              'onchange' => "toggleSelectAll(this);");
          $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#type'] = "checkbox";
          $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#return_value'] = $j+1;
          unset($form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['_weight']);
        }
        else{
          //Change type to checkbox
          $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#type'] = "checkbox";
          $form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['value']['#return_value'] = $j+1;
          unset($form['budget']['table_wrapper']['table'][$row_name]['und'][$j]['_weight']);
        }
      }
      //Remove row delta that is not used
      if(count($budget_categories) == 0){
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][0]);
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][1]);
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][2]);
      }
      if(count($budget_categories) == 1){
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][1]);
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][2]);
      }
      if(count($budget_categories) == 2){
        unset($form['budget']['table_wrapper']['table'][$row_name]['und'][2]);
      }
    }
  }

  if(!$fp_visible){
    $form['budget']['table_wrapper']['#collapsed'] = TRUE;
    $form['budget']['table_wrapper']['#states'] = array('visible' => FALSE);
  }
  else{
    $form['budget']['table_wrapper']['#collapsed'] = FALSE;
    $form['budget']['table_wrapper']['#states'] = array('visible' => TRUE);
  }

  //Remove budget table fields from form
  unset($form["field_fun_categories"]);
  unset($form["field_fun_pm"]);
  unset($form["field_fun_pc"]);
  unset($form["field_fun_ts"]);
  unset($form["field_fun_cs"]);
  unset($form["field_fun_eq"]);
  unset($form["field_fun_sc"]);
  unset($form["field_fun_oc"]);
  unset($form["field_fun_ic"]);
  unset($form["field_fun_rm"]);
  return $form;
}