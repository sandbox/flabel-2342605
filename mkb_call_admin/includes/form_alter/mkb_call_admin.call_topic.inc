<?php
/**
 * @file
 * Code for the mkb_call_admin.call_topic.
 */
module_load_include('inc', 'mkb_call_admin', 'includes/mkb_call_admin.helper');

function mkb_call_admin_call_topic($form, $form_state, $form_id){
  $i=0;
  $call_topic_index=0;
  $edit_index=0;
  $url_comp = explode('/', request_uri());

  foreach($url_comp as $comp) {
    if ($comp == 'call-topic') $call_topic_index = $i;
    if ($comp == 'edit') $edit_index = $i;
    if (strpos($comp,'?')) $url_comp[$i] = substr($comp,0,strpos($comp,'?'));
    $i++;
  }

  // Set destination, with Call id if available from URL
  // Hide field Call id if set
  $destination = 'call-topics-setup';
  if ($edit_index > 0 && isset($url_comp[$edit_index+1])) {
    $destination = 'call-topics-setup/' . $url_comp[$edit_index+1];
    $form['field_call_id']['#access'] = false;
  }
  if ($call_topic_index > 0 && isset($url_comp[$call_topic_index+1])) {
    $destination = 'call-topics-setup/' . $url_comp[$call_topic_index+1];
    $form['field_call_id']['#access'] = false;
  }
  $_GET['destination'] = $destination;

  if ($call_topic_index == 0) unset($url_comp); // Default values is set only when "add"
  if (isset($url_comp[$call_topic_index+1])) { // Call id is in URL
    $form['field_call_id']['und']['#default_value'] = $url_comp[$call_topic_index+1];
  }
  if (isset($url_comp[$call_topic_index+2])) { // Parent id is in URL
    if ($url_comp[$call_topic_index+2] > 0) $form['field_topic_parent']['und']['#default_value'] = $url_comp[$call_topic_index+2];
  }
  if (isset($url_comp[$call_topic_index+3])) { // Level is in URL
    $form['field_topic_level']['und'][0]['value']['#default_value'] = $url_comp[$call_topic_index+3];
  }
  if (isset($url_comp[$call_topic_index+4])) { // Weight id is in URL
    $form['field_topic_weight']['und'][0]['value']['#default_value'] = $url_comp[$call_topic_index+4];
  }
  // These fields have a default from URL or from content type settings
  $form['field_topic_parent']['#access'] = false;
  $form['field_topic_level']['#access'] = false;
  if ($edit_index == 0) $form['field_topic_weight']['#access'] = false;  //hide only when add, not edit
  if ($edit_index > 0) $form['field_call_id']['#access'] = false;  //hide when edit
  return $form;
}