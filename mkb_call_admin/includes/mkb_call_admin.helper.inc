<?php
/**
 * @file
 * Code for the mkb_call_admin.helper.
 */

/**
  * Param Call-main-page nid
  * Return funding agency options
 */
function mkb_call_admin_get_fa_options($nid){
  $query = db_select("node", "n");
  $query->leftJoin("field_data_field_call_id", "ci", "ci.entity_id = n.nid");
  $query->leftJoin("field_data_field_funder_page", "fp", "fp.entity_id = n.nid");
  $query
  ->fields("n", array("title", "nid"))
  ->condition("ci.field_call_id_target_id", $nid)
  ->condition("n.type", "call_funder_page");
  $result = $query->execute()->fetchAll();
  $options = array("_none" => "- Select -");
  foreach($result as $row){
    $options[$row->nid] = $row->title;
  }
  return $options;
}

/**
 * Returns values aliased as "delta" and "value" from a multiple value field
 */
function mkb_call_admin_get_values($nid, $field_name) {
  $query = db_select("field_data_field_" . $field_name, "x");
  $query->addField("x", "delta", "delta");
  $query->addField("x", "field_" . $field_name . "_value", "value");
  $query->condition("x.entity_id", $nid, "=");
  $result = $query->execute()->fetchAll();
  return $result;
}

function mkb_call_admin_get_topics_from_call_id($nid){
  $query = db_select("field_data_field_call_id", "ci");
  $query->leftJoin("node", "n", "ci.entity_id = n.nid");
  $query->leftJoin("field_data_field_topic_level", "tl", "n.nid = tl.entity_id");
  $query->leftJoin("field_data_field_topic_weight", "tw", "tl.entity_id = tw.entity_id");
  $query->addField("n", "title", "title");
  $query->addField("tw", "field_topic_weight_value", "weight");
  $query->condition("ci.field_call_id_target_id", $nid, "=");
  $query->condition("ci.bundle", "call_topic", "=");
  $query->condition("tl.field_topic_level_value", 1, "=");
  $query->orderBy('weight');
  $result = $query->execute()->fetchAll();
  return $result;
}