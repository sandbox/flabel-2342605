<div class="expert_pages">
<?php
print '<table><tr>';
print '<td><b>Conflict: </b>';
print '<ul class="no_style">';
print '<li>' . $variables['images']['con_missing_img'] . ' Missing</li>' ;
print '<li>' . $variables['images']['con_no_img'] . ' Declared no conflict</li>' ;
print '<li>' . $variables['images']['con_yes_img'] . ' Declared conflict</li>' ;
print '</ul></td>';

print '<td><b>Scientific evaluation: </b>';
print '<ul class="no_style">';
print '<li>' . $variables['images']['eval_missing_img'] . ' Missing</li>' ;
print '<li>' . $variables['images']['eval_approved_img'] . ' Approved</li>' ;
print '<li>' . $variables['images']['eval_eval_approved_wc_img'] . ' Approved with conditions</li>' ;
print '<li>' . $variables['images']['eval_not_approved_img'] . ' Not approved</li>' ;
print '</ul></td>';

print '<td><b>Sector and rapporteur evaluation: </b>';
print '<ul class="no_style">';
print '<li>' . $variables['images']['eval_missing_img'] . ' Missing</li>' ;
print '<li>' . $variables['images']['eval_created_img'] . ' Created</li>' ;
print '</ul></td>';
print '</tr></table>';

// dpm($variables);
$table = array(
    'header' => $variables['header'],
    'rows' => $variables['rows'],
    'attributes' => array(
        'class' => array('expert_evaluators_table'),
        'width' => '100%',
        ),
    'sticky' => TRUE,
    'empty' => 'There are no evaluation experts assigned to any applications yet.',
    'colgroups' => array(),
    'caption' => '',
    );
print theme_table($table);
?>
</div>
