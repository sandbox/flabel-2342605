<?php
// dpm($variables);
print '<div class="evaluation_pages">';
print '<h2>' . $variables['view_title'] . ' (' . $variables['count'] . ')' . '</h2>';
$table = array(
    'header' => $variables['header'],
    'rows' => $variables['content'],
    'attributes' => array(
        'class' => array('table_class'),
        'width' => '100%',
        ),
    'sticky' => TRUE,
    'empty' => 'No applications fulfill the filter for this page.',
    'colgroups' => array(),
    'caption' => $variables['view_description'],
    );
print theme_table($table);
print '</div>';
?>

