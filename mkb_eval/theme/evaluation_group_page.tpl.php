<?php
print '<div class="evaluation_pages">';
print '<h2>' . $variables['group']['gid'] . ' ' . $variables['group']['acronym'] .  ' ' . $variables['group']['title'] . '</h2>';
foreach ($variables['content'] as $eval_type){
  print '<h3>'. $eval_type['title'] . '</h3>';
  foreach ($eval_type['data'] as $eval){
  $table = array(
      'header' => $eval['header'],
      'rows' => $eval['rows'],
      'attributes' => array(
          'class' => array('table_class'),
          'width' => '50%',
          ),
      'sticky' => FALSE,
      'empty' => 'No evaluations of this type.',
      'colgroups' => array(),
      'caption' => $eval['author'],
      );
  print theme_table($table);
  print '<p>Recommendation: <b>' . $eval['recommendation'] . '</b></p>';
  print '<p>Comment: ' . $eval['comment'] . '</p>';
  print '<br>';
  }
}
print '</div>';
?>

