<?php
$topics = $variables['topics'];
$rows = $variables['rows'];

print '<div class="evaluation_pages">';
// print '<h2 class="block-title">Rapporteur evaluations for stage 2</h2>';
print '</br>';

foreach ($rows as $topic_id => $topic_applications){
  $row_topic = $topics[$topic_id];

  print '<p><b><i>' . $row_topic->title . '</i></b></p>';

  foreach ($topic_applications as $application){
    $table = array(
        'header' => array($application['application']),
        'rows' => $application['rapports'],
        'attributes' => array(
            'class' => array('table_class'),
            'width' => '50%',
            ),
        'sticky' => TRUE,
        'empty' => 'Missing',
        'colgroups' => array(),
        'caption' => '',
        );
    print theme_table($table);
    print '<br>';
  }
}
print '</div>';
?>

