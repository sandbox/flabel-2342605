<?php
print '<div class="evaluation_pages">';
print '<h2>' . $variables['topic'] . '</h2>';
foreach ($variables['content'] as $gid => $group){
  $caption = '<div class="application">' . $gid . ' ' . $group['acronym'] . '</br>' .$group['title'] . '</div>';
  if(isset($group['tables'])){
    $set_caption = true;
    foreach ($group['tables'] as $type => $table){
      // Build rows
      $arr_rows = array();
      $i = 0;
      foreach ($table['rows'] as $row){
	    // Answers only in quality evaluation
        if ($type!=='quality_evaluation') $row['answers'] = ' ';
        // Create each row
        $row_data = array();
        $j = 0;
        foreach ($row as $field){
          // Create each field
          $field_to_add = array(
              'data' => $field,
              );

          if($j==0){
            $field_to_add['class'] = 'width_50';
          }
          elseif($j==1){
            $field_to_add['class'] = 'width_50';
          }
          elseif($j==2){
            $field_to_add['class'] = 'width_60';
          }
          elseif($j==3){
            $field_to_add['class'] = 'comment';
          }

          // Add each field to rows
          $row_data[$j] = $field_to_add;
          $j++;
        }
        $arr_rows[$i] = array('data' => $row_data, 'class' => array());
        $i++;
      }


      $table_print = array(
          'header' => array(
              0 => array(
                'data' => '<div class="eval_type">' . $table['header'] . '</div>',
                'colspan' => 4,
                ),
              ),
          'rows' => $arr_rows,
          'attributes' => array(
              'class' => array('table_auto_width'),
              ),
          'sticky' => FALSE,
          'empty' => 'No evaluations of this type.',
          'colgroups' => array(),
          'caption' => '',
          );
      if ($set_caption) {
  	    $table_print['caption'] = $caption;
        $set_caption = false;
	  }
	  print theme_table($table_print);
    }
    print '</br>';
  }
}
print '</div>';
?>

