<?php
/**
 * @file
 * mkb_eval.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mkb_eval_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mkb_eval_views_api($module = NULL, $api = NULL) {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'mkb_eval'),
    'template path' => drupal_get_path('module', 'mkb_eval') . '/theme',
  );
}

/**
 * Implements hook_default_evaluation_type().
 */
function mkb_eval_default_evaluation_type() {
  $items = array();
  $items['evaluation_a'] = entity_import('evaluation_type', '{
    "type" : "evaluation_a",
    "label" : "Evaluation A",
    "weight" : "0",
    "data" : { "sample_data" : 0 },
    "rdf_mapping" : []
  }');
  $items['evaluation_b'] = entity_import('evaluation_type', '{
    "type" : "evaluation_b",
    "label" : "Evaluation B",
    "weight" : "0",
    "data" : { "sample_data" : 0 },
    "rdf_mapping" : []
  }');
  $items['evaluation_c'] = entity_import('evaluation_type', '{
    "type" : "evaluation_c",
    "label" : "Evaluation C",
    "weight" : "0",
    "data" : { "sample_data" : 0 },
    "rdf_mapping" : []
  }');
  $items['evaluation_d'] = entity_import('evaluation_type', '{
    "type" : "evaluation_d",
    "label" : "Evaluation D",
    "weight" : "0",
    "data" : { "sample_data" : 0 },
    "rdf_mapping" : []
  }');
  $items['evaluation_e'] = entity_import('evaluation_type', '{
    "type" : "evaluation_e",
    "label" : "Evaluation E",
    "weight" : "0",
    "data" : { "sample_data" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function mkb_eval_node_info() {
  $items = array(
    'eligible_evaluation' => array(
      'name' => t('Eligible evaluation'),
      'base' => 'node_content',
      'description' => t('Add a eligible evaluation scheme to a call.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'evaluation_conflict' => array(
      'name' => t('Declaration of no conflict'),
      'base' => 'node_content',
      'description' => t('One to One acknowledgement of any conflict between the "Expert evaluator" and the group applicants'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'expert_evaluation' => array(
      'name' => t('Expert evaluation'),
      'base' => 'node_content',
      'description' => t('Add a Expert evaluation scheme to a call.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'expert_evaluators' => array(
      'name' => t('Expert evaluators'),
      'base' => 'node_content',
      'description' => t('Evaluators for stage 2'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'form_eval' => array(
      'name' => t('Formality evaluation'),
      'base' => 'node_content',
      'description' => t('Add a formality evaluation scheme to a call.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'quality_evaluation' => array(
      'name' => t('Quality evaluation'),
      'base' => 'node_content',
      'description' => t('Add a Quality evaluation scheme to a call.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rapporteur_evaluation' => array(
      'name' => t('Rapporteur evaluation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'scientific_evaluation' => array(
      'name' => t('Scientific evaluation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sector_evaluation' => array(
      'name' => t('Sector evaluation'),
      'base' => 'node_content',
      'description' => t('Configuration node for Evaluation D (sector evaluation).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
