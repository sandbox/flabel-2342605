function Totals(){
	rows = document.getElementById('application_selection_table').getElementsByTagName("TR");

	hdata = new Array();
	for (var i = 2; i < 4; i++) {
		hcells = rows[i].getElementsByTagName('TH');
		hdata[i-2] = new Array(hcells.length-1);
	    for (var j = 1; j < hcells.length; j++) {
	    	hdata[i-2][j-1] = 0;
//	    	hcells[j].innerHTML = 0;
//	    	hcells[j].className = "text_black";
	    }
	}
	
//	alert('rows: ' + hdata.length);
//	alert('cols: ' + hdata[0].length);
	for (var i = 5; i < rows.length; i++) {
	    rcells = rows[i].getElementsByTagName('TD');

	    checked = false;
    	if(rcells[0].getElementsByTagName('input')[0].checked){
    		checked = true;
    	}
	    for (var j = 5; j < rcells.length-1; j++) {
	    	if(checked){
	    		// requested funding
	    		hdata[0][j-5] = hdata[0][j-5] + parseInt(rcells[j].innerHTML);
	    		// Deficit
	    		hcells_j = j-4;
	    		avaliable = parseInt(document.getElementById('cell_1_' + hcells_j).innerHTML);
	    		hdata[1][j-5] = avaliable-hdata[0][j-5];
	    	}
	    }
	}
	
    requested_total = 0;
    deficit_total = 0;
	for (var i = 2; i < 4; i++) {
		hcells = rows[i].getElementsByTagName('TH');
	    for (var j = 1; j < hcells.length; j++) {
	    	// Set requested funding and deficit
	    	hcells[j].innerHTML = hdata[i-2][j-1];
	    	if(hdata[i-2][j-1]<0){
		    	hcells[j].className = "text_red";
	    	}
	    	else{
		    	hcells[j].className = "text_black";	    		
	    	}
	    }
	}
}

