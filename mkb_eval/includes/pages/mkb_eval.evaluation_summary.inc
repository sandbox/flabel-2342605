<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');

/**
 * Generates content for evaluation_summary_page template.
 *
 * @param $cid
 *   Call-main-page nid.
 * @param $tid
 *   Call topic nid.
 * @return template
 *   The template evaluation_summary_page_template is called with $content.
 */
function evaluation_summary_page_function($cid, $tid){
  // Get quality_evaluation
  $eid = mkb_eval_get_node_type('quality_evaluation');
  $eval_conf = node_load($eid);

  $group_ids = mkb_eval_get_group_ids($cid);
  $groups = mkb_eval_build_groups($group_ids);

  // Unset all groups that doesnt have this topic selected
  foreach ($groups as $gid => $group){
    $has_topic = mkb_eval_has_topic($group['application_project_info'], $tid);
    if($has_topic == FALSE){
      unset($groups[$gid]);
    }
  }

  // Approved in formality evaluations
  foreach ($groups as $gid => $group){
    $form_eval_ids = mkb_eval_get_evaluation_ids($gid, 'form_eval');
    $unset = TRUE;
    if($form_eval_ids){
      foreach ($form_eval_ids as $form_eval_id){
        $evaluation = evaluation_load($form_eval_id->evaluation_id);
        if($evaluation->recommendation() == 'Approved'){
          $unset = FALSE;
        }
      }
    }
    if($unset){
      unset($groups[$gid]);
    }
  }

  $content = array();
  foreach ($groups as $gid => $group){
    $evaluation_ids = mkb_eval_get_evaluation_ids($gid);

    $content[$gid]['acronym'] = mkb_eval_get_acronym($group['application_project_info']);
    $content[$gid]['title'] = mkb_eval_get_title($group['application_project_info']);

    $i=0;
    foreach ($evaluation_ids as $evaluation_index => $evaluation_id){
      $evaluation = evaluation_load($evaluation_id->evaluation_id);
      $eval_conf = node_load($evaluation->eid);
      $type_letter = substr($evaluation->type, -1);

      // Title = Evaluation type
//       $content[$gid][$eval_conf->type]['type'] = $eval_conf->title;

      // Data from evaluation
      $content[$gid]['tables'][$eval_conf->type]['header'] =  $eval_conf->title;

      $answers = '';
      foreach ($evaluation->{'field_answer_' . $type_letter}['und'] as $answer_index => $answer){
        $answers .= ' ' . $answer['value'];
      }

      $author = user_load($evaluation->uid);
      // Country
      $funder_pages = array_merge(mkb_eval_get_manager_funder_pages($cid, $author->uid),
                            mkb_eval_get_evaluator_funder_pages($cid, $author->uid));

      $content[$gid]['tables'][$eval_conf->type]['rows'][$i]['country'] = mkb_eval_get_funder_page_country(end($funder_pages));
      // Recommendation
      $content[$gid]['tables'][$eval_conf->type]['rows'][$i]['recommendation'] = isset($evaluation->recommendation) ? $evaluation->recommendation() : '';
      // Answers
      $content[$gid]['tables'][$eval_conf->type]['rows'][$i]['answers'] = $answers;
      // Comment
      $content[$gid]['tables'][$eval_conf->type]['rows'][$i]['comment'] = isset($evaluation->comment) ? $evaluation->comment : '';
      $i++;
    }
  }

  // Set topic text
  $topic = node_load($tid);
  $variables['topic'] = $topic->title;
  $variables['content'] = $content;
  return theme('evaluation_summary_page_template', $variables);
}