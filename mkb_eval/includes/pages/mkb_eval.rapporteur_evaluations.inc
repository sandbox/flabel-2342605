<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');

/**
 * Generates content for evaluation_summary_page template.
 *
 * @param $cid
 *   Call-main-page nid.
 * @return template
 *   The template application_partners_template is called with $content.
 */
function mkb_eval_rapporteur_evaluations($cid){
  $variables = array();
  $rows = array();

  // Get all topics
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'call_topic')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_topic_level', 'value', 1, '=')
    ->fieldOrderBy('field_topic_weight', 'value', 'ASC');

  $result = $query->execute();

  $topics = array();
  if (!empty($result['node'])) {
    $nids = array_keys($result['node']);
    $nodes = node_load_multiple($nids);

    foreach ($nodes as $node) {
      $topics[$node->nid] = $node;
      $rows[$node->nid] = array();
    }
  }

  // Get groups selected for stage 2
  $groups = mkb_eval_get_stage_2_applications();
  foreach ($groups as $gid => $group){
    // Get application-info
    $project_info_nids = mkb_application_get_group_content($gid, 'application_project_info');
    if($project_info_nids){
      $project_info_node = node_load($project_info_nids[0]);
      $project_info_wrapper = entity_metadata_wrapper('node', $project_info_node);

      // Topic
      $project_info_topic = $project_info_wrapper->field_app_topics[0]->value();

      // Application
      $rows[$project_info_topic->nid][$gid]['application'] = $gid . ' ' .
        $project_info_wrapper->field_app_acronym->value();

      // Rapporteurs
      $evaluation_ids = mkb_eval_get_evaluation_ids($gid, 'rapporteur_evaluation');
      foreach ($evaluation_ids as $evaluation_id){
        $rapport = '';

        // Get data from evaluation
        $evaluation = evaluation_load($evaluation_id->evaluation_id);
        $evaluation_wrapper = entity_metadata_wrapper('evaluation', $evaluation);
        $field_rapport = field_view_field('evaluation', $evaluation, 'field_ra_rapport');

        $uid = $evaluation_wrapper->uid->value();
        $rapporteur = user_load($uid);

        $rapport = '<p><b>Rapporteur: </b>' . $rapporteur->realname .
            '</br><b>Score: </b>' . $evaluation_wrapper->field_score->value() . '</br>' .
            render($field_rapport) . '</p>';

        $rows[$project_info_topic->nid][$gid]['rapports'][] = array(
            'rapport' => array('data'=>$rapport, 'width'=>'100%'),
            );
      }
    }
  }

  $variables['topics'] = $topics;
  $variables['rows'] = $rows;
  return theme('rapporteur_evaluations', $variables);
}


