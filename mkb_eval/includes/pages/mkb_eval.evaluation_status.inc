<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');

/**
 * Generates content for evaluation_status_page template.
 *
 * @param $cid
 *   Call-main-page nid.
 * @param $eid
 *   Evaluation configuration node id.
 * @return template
 *   The template evaluation_status_page_template is called with $variables.
 */
function evaluation_status_page_function($cid, $eid){
  $eval_conf = node_load($eid);

  $group_ids = mkb_eval_get_group_ids($cid);
  $groups = mkb_eval_build_groups($group_ids);

  // Build header (All funding agencies)
  $funding_agencies = mkb_eval_get_all_funding_agencies($cid);

  // Build content (Building groups removes groups that does not have a project-info node
  // so we have to get the keys from groups)
  $content = array();
  foreach ($groups as $gid => $group){
    $content[$gid] = array(
        'gid' => $gid,
        'acronym' => mkb_eval_get_acronym($group['application_project_info']),
        );

    $groups[$gid]['funder_pages'] = array();
    if(isset($group['application_partner_budget'])){
      foreach ($group['application_partner_budget'] as $budget_id){
        $funder_page = mkb_eval_get_funder_page($budget_id);
        if(!in_array($funder_page, $groups[$gid]['funder_pages'])) {
          $groups[$gid]['funder_pages'][] = $funder_page;
        }
      }
    }

    foreach ($funding_agencies as $fid => $funding_agency){
      $content[$gid][$fid] = "Nothing";
      if(in_array($fid, $groups[$gid]['funder_pages'])){
        $content[$gid][$fid] = "Pending";
        $funder_page_uids = array_merge(mkb_eval_get_uids_manager_funder_pages($fid),
            mkb_eval_get_uids_evaluator_funder_pages($fid));

        $eval_id = mkb_eval_get_funder_page_evaluation_id($funder_page_uids, $eval_conf, $gid);
        if($eval_id){
          $evaluation = evaluation_load($eval_id);
          $content[$gid][$fid] = isset($evaluation->recommendation) ? $evaluation->recommendation() : '';
		  $groups[$gid]['eval_id'] = $eval_id;
        }
      }
    }
  }
  $variables['cid'] = $cid;
  $variables['header'] = array_merge(array("", ""), $funding_agencies);
  $variables['groups'] = $groups;
  $variables['content'] = $content;
  return theme('evaluation_status_page_template', $variables);
}