<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.custom_views');
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.custom_filters');

/**
 * Generates content for evaluation_page template.
 *
 * @param $cid
 *   Call-main-page nid.
 * @param $vid
 *   View id: Determines which fields are in content
 * @param $arg3 (optional)
 *   Evaluation configuration node id OR Filter id
 * @param $arg4 (optional)
 *   Filter id: Determines which groups are in the content
 * @return template
 *   The template evaluation_page_template is called with $content.
 */
function evaluation_page_function($cid, $vid, $arg3 = NULL, $arg4 = NULL){
  // GLOBALS
  global $user;
  $base_url = $GLOBALS['base_url'];

  // Determine optional arguments
  $eid = NULL;
  $fid = NULL;
  if($arg3 !== NULL){
    $arg3_strings = explode("=", $arg3);
    if($arg3_strings[0] == "eid"){
      $eid = $arg3_strings[1];
    }
    elseif($arg3_strings[0] == "fid"){
      $fid = $arg3_strings[1];
    }
    else{
      drupal_set_message(t('Invalid third argument. Valid argument example: base_url/eval/cid/vid/eid=x'));
    }
  }
  if($arg4 !== NULL){
    $arg4_strings = explode("=", $arg4);
    if($arg4_strings[0] == "fid"){
      $fid = $arg4_strings[1];
    }
    else{
      drupal_set_message(t('Invalid fourth argument. Valid argument example: base_url/eval/cid/vid/eid=x/fid=y'));
    }
  }

  // Config evaluation node
  if($eid !== NULL){
    $eval_conf = node_load($eid);

    // Get the type from evaluation configuration node
    switch ($eval_conf->type) {
        case "form_eval":
            $type = 'b';
            break;
        case "eligible_evaluation":
            $type = 'b';
            break;
        case "quality_evaluation":
            $type = 'a';
            break;
        case "scientific_evaluation":
            $type = 'c';
            break;
        case "sector_evaluation":
            $type = 'd';
            break;
        case "rapporteur_evaluation":
            $type = 'e';
            break;
//         case "expert_evaluation":
//             $type = 'a';
//             break;
    }

    $eval_type = "evaluation_" . $type;
  }

  // Get all group ids to this call
  $group_ids = mkb_eval_get_group_ids($cid);

  // Get Predefined views from mkb_eval.custom_views.inc
  $view_types = mkb_eval_get_custom_views();

  // Set the current view
  // Initiate each group in content with fields that are true in current view
  $view = array();
  $view_keys = array();
  if(array_key_exists($vid, $view_types)){
    $view = $view_types[$vid];

    // Build group_default = an empty array with keys from current view
    $view_keys = array_keys($view, TRUE, TRUE);
  }
  else {
    $variables['content'] = FALSE;
    return theme('evaluation_page_template', $variables);
  }

  // Get headers from mkb_eval.custom_views.inc
  $labels = mkb_eval_get_view_labels();

  // Build headers for fields that are true in current view (view_keys)
  $header = array();
  foreach ($view_keys as $key){
    $header[] = $labels[$key];
  }

  // Build groups
  $groups = mkb_eval_build_groups($group_ids);

  // Get group_content (gid, group title and group body)
  $group_content = mkb_eval_get_group($cid, array('group_title' => TRUE, 'group_body' => TRUE));

  // Build content (Building groups removes groups that does not have a project-info node
  // so we have to get the keys from groups)
  $content = array();
  foreach (array_keys($groups) as $gid){
    $content[$gid] = "";
  }
  $content = mkb_eval_build_content($content, $group_content, $view, $view_keys);

  // Filter
  if($fid !== NULL){
    // Get predefined filters from mkb_eval.custom_filters.inc
    $filters = mkb_eval_get_custom_filters();

    // Load filter
    if(array_key_exists($fid, $filters)){
      $filter = $filters[$fid];
    }

    // Filter groups
    // Submitted
    if($filter['submitted']){
      foreach ($groups as $gid => $group){
        if(isset($group['application_project_info'])){
          $submitted = mkb_eval_get_submitted($group['application_project_info']);
          if($submitted == 0){
            unset($groups[$gid]);
            unset($content[$gid]);
          }
        }
        else{
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }

    // Selected_1
    if($filter['selected_1']){
      foreach ($groups as $gid => $group){
        if(isset($group['application_project_info'])){
          $selected_1 = mkb_eval_get_selected_1($group['application_project_info']);
          if($selected_1 == 0){
            unset($groups[$gid]);
            unset($content[$gid]);
          }
        }
        else{
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }

    // Approved in formality evaluations
    if($filter['approved_in_formality']){
      foreach ($groups as $gid => $group){
        $form_eval_ids = mkb_eval_get_evaluation_ids($gid, 'form_eval');
        $unset = TRUE;
        if($form_eval_ids){
          foreach ($form_eval_ids as $form_eval_id){
            $evaluation = evaluation_load($form_eval_id->evaluation_id);
            if($evaluation->recommendation() == 'Approved'){
              $unset = FALSE;
            }
          }
        }
        if($unset){
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }

    // Approved in all eligible evaluations
    if($filter['approved_in_eligible']){
      foreach ($groups as $gid => $group){
        $elig_eval_ids = mkb_eval_get_evaluation_ids($gid, 'eligible_evaluation');
        if($elig_eval_ids){
          foreach ($elig_eval_ids as $elig_eval_id){
            $evaluation = evaluation_load($elig_eval_id->evaluation_id);
            if($evaluation->recommendation() !== 'Approved'){
              unset($groups[$gid]);
              unset($content[$gid]);
            }
          }
        }
        else{
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }

    // Funding agency in partner budget:
    // Current user is 'call manager' in funding agency used in this groups partner budget as funding agency.
    if($filter['funding_agency_in_partner_budget']){
      $funder_pages = array_merge(mkb_eval_get_manager_funder_pages($cid, $user->uid),
                                  mkb_eval_get_evaluator_funder_pages($cid, $user->uid));
      $funder_pages = array_unique($funder_pages);
      $funder_pages = array_values($funder_pages);
      if($funder_pages) $partner_budgets = mkb_eval_get_partner_budgets($funder_pages);
      foreach ($groups as $gid => $group){
        if(isset($group['application_partner_budget']) && isset($partner_budgets)){
          $unset = TRUE;
          foreach ($group['application_partner_budget'] as $budget_nid){
            if(in_array($budget_nid, $partner_budgets)){
              $unset = FALSE;
            }
          }
          if($unset){
            unset($groups[$gid]);
            unset($content[$gid]);
          }
        }
        else{
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }

    // Show only the groups this expert evaluator is assigned to
    if($filter['experts_assigned']){
      foreach ($groups as $gid => $group){
        $expert_evaluators = mkb_eval_get_aid($gid, 'expert_evaluators');
        $unset = TRUE;
        if($expert_evaluators){
          $expert_evalators_entity = node_load($expert_evaluators);
          $wrapper = entity_metadata_wrapper('node', $expert_evalators_entity);
          foreach ($wrapper->field_ee_evaluator->getIterator() as $delta => $user_wrapper) {
            if($user->uid == $user_wrapper->getIdentifier()) $unset = FALSE;
          }
        }
        if($unset){
          unset($groups[$gid]);
          unset($content[$gid]);
        }
      }
    }
    // END IF $fid !== NULL
  }

  // Group evaluation
  foreach ($groups as $gid => $group){
      // Add current user evaluations if any
    if($eid !== NULL){
      if($eval_conf->type == 'form_eval'){
        $eval_id = mkb_eval_form_eval_link($gid);
      }
      elseif($eval_conf->type == 'eligible_evaluation' || $eval_conf->type == 'quality_evaluation'){
        $funder_pages = array();
        $funder_pages = array_merge(mkb_eval_get_manager_funder_pages($cid, $user->uid),
                                    mkb_eval_get_evaluator_funder_pages($cid, $user->uid));
        $funder_pages = array_unique($funder_pages);
        $funder_pages = array_values($funder_pages);

        if(isset($funder_pages)){
          $funder_page_uids = array();
          $funder_page_uids = array_merge(mkb_eval_get_uids_manager_funder_pages($funder_pages[0]),
                                        mkb_eval_get_uids_evaluator_funder_pages($funder_pages[0]));
        }
        $eval_id = mkb_eval_get_funder_page_evaluation_id($funder_page_uids, $eval_conf, $gid);
      }
      elseif($eval_conf->type == 'scientific_evaluation' || $eval_conf->type == 'sector_evaluation' ||
          $eval_conf->type == 'rapporteur_evaluation'){
        $expert_evaluators = mkb_eval_get_aid($gid, 'expert_evaluators');
        if($expert_evaluators){
          $expert_evalators_entity = node_load($expert_evaluators);
          $wrapper = entity_metadata_wrapper('node', $expert_evalators_entity);
          foreach ($wrapper->field_ee_evaluator->getIterator() as $delta => $user_wrapper) {
            if($user->uid == $user_wrapper->getIdentifier()){
              $eval_id = mkb_eval_get_evaluation_id($user->uid, $eid, $gid, $eval_type);
            }
          }
        }
      }
      else{
//         $eval_id = mkb_eval_get_evaluation_id($user->uid, $eid, $gid, $eval_type);
        $eval_id = FALSE;
      }
      if($eval_id !== FALSE){
        $evaluation = evaluation_load($eval_id);
        $groups[$gid]['link_evaluation'] = $evaluation;
      }
    }
  }

  // Fill content fields
  // Application-project-info
  // Acronym
  if($view['acronym']){
    foreach ($groups as $gid => $group){
      if(isset($group['application_project_info'])){
        $acronym_text = mkb_eval_get_acronym($group['application_project_info']);
        $acronym = l($acronym_text, $base_url . '/iprojects/workspace/application_page/' . $gid);
      }
      else{
        $acronym = '';
      }
      $content[$gid]['acronym'] = $acronym;
    }
  }

  // Title
  if($view['title']){
    foreach ($groups as $gid => $group){
      if(isset($group['application_project_info'])){
        $title = mkb_eval_get_title($group['application_project_info']);
      }
      else{
        $title = '';
      }
      $content[$gid]['title'] = $title;
    }
  }

  // Submitted
  if($view['submitted']){
    foreach ($groups as $gid => $group){
      if(isset($group['application_project_info'])){
        $is_submitted = mkb_eval_get_submitted($group['application_project_info']);
        if($is_submitted == 0) $submitted = "No";
        if($is_submitted == 1) $submitted = "Yes";
      }
      else{
        $submitted = '';
      }
      $content[$gid]['submitted'] = $submitted;
    }
  }

  // Coordinator (The project-coordinator is the only role who can create a application-project-info).
  if($view['coordinator']){
    foreach ($groups as $gid => $group){
      $this_group = node_load($gid);
      $coordinator = user_load($this_group->uid);
      if(isset($coordinator)){
        $content[$gid]['coordinator'] = l($coordinator->realname, $base_url . '/user-profile/' . $coordinator->uid);
      }
      else{
        $content[$gid]['coordinator'] = '';
      }
    }
  }

  // DOW
  if($view['dow']){
    foreach ($groups as $gid => $group){
      if(isset($group['application_project_info'])){
        $dow = mkb_eval_get_dow($group['application_project_info']);
      }
      else{
        $dow = '';
      }
      $content[$gid]['dow'] = $dow;
    }
  }

  // Topics
  if($view['topics']){
    foreach ($groups as $gid => $group){
      if(isset($group['application_project_info'])){
        $topics = mkb_eval_get_topics($group['application_project_info']);
      }
      else{
        $topics = '';
      }
      $content[$gid]['topics'] = $topics;
    }
  }

  // Applicaiton-partner-info
  //Country
  if($view['country']){
    $country = '';
    foreach ($groups as $gid => $group){
      if(!empty($group['application_partner_info'])){
        $country = mkb_eval_get_country($gid);
      }
      $content[$gid]['country'] = $country;
    }
  }

  // Application-partner-budget
  // Total budget && Total requested
  if($view['total_budget'] && $view['total_requested']){
    foreach ($groups as $gid => $group){
      $total_budget = 0;
      $total_requested = 0;
      if(isset($group['application_partner_budget'])){
        foreach ($group['application_partner_budget'] as $nid){
          $budget_table = mkb_application_get_budget_table($nid);
          $sum_index = count($budget_table)-1;
          $total_budget += $budget_table[$sum_index][count($budget_table[$sum_index])-2];
          $total_requested += $budget_table[$sum_index][count($budget_table[$sum_index])-1];
        }
      }
      else{
        $total_budget = '';
        $total_requested = '';
      }
      if($total_budget == 0) $total_budget = '';
      if($total_requested == 0) $total_requested = '';
      $content[$gid]['total_budget'] = $total_budget;
      $content[$gid]['total_requested'] = $total_requested;
    }
  }

  // Status
  if($view['status']){
    if($eid !== NULL){
      foreach ($groups as $gid => $group){
        $recommendation = 'Pending';
        if(isset($group['link_evaluation'])){
          $evaluation = $group['link_evaluation'];
          $recommendation = $evaluation->recommendation();
        }
        $content[$gid]['status'] = $recommendation;
      }
    }
    else{
      drupal_set_message(t('Status is true in view but eid not given as argument.'));
    }
  }

  // Link
  if($view['link']){
    if($eid !== NULL){
      foreach ($groups as $gid => $group){
        if(($eval_conf->type == 'scientific_evaluation' && in_array('scientific expert', $user->roles)) ||
              ($eval_conf->type == 'sector_evaluation' && in_array('sector expert', $user->roles)) ||
              ($eval_conf->type == 'rapporteur_evaluation' && in_array('rapporteur', $user->roles))){

          if(isset($group['link_evaluation'])){
            $evaluation = $group['link_evaluation'];
            $link = l(t('Edit'), $base_url .
            '/admin/content/evaluations/evaluation/'. $evaluation->evaluation_id . '/edit',
            array('query' => drupal_get_destination()));
          }
          else{
            // Check for if Evaluation conflict has been created for this Expert evaluator
            if($view['conflict']){
              $conflict_nid  = mkb_eval_get_eval_conflict($gid, $user->uid);
              // Conflict node exist
              if($conflict_nid){
                $conflict_node = node_load($conflict_nid);
                $is_conflict = isset($conflict_node->field_con_conflict['und'][0]['value']) ?
                  $conflict_node->field_con_conflict['und'][0]['value'] : 0;

                // There is a conflict
                if($is_conflict == 0){
                   $link = l(t('Create'), $base_url .
                  '/admin/content/evaluations/add/'. $eval_type . '/' . $eid . '/' . $gid,
                    array('query' => drupal_get_destination()));
                }
                // There is no conflict
                else{
                  $link = '';
                }
                $conflict = '';
              }
              // Conflict node does not exist
              else{
                $link = '';
                $conflict = l(t('Create'), $base_url . '/node/add/evaluation-conflict/' . $gid,
                  array('query' => drupal_get_destination()));
              }
              $content[$gid]['conflict'] = $conflict;
            }
            // All other Evalation types than Expert
            else{
              $link = l(t('Create'), $base_url .
              '/admin/content/evaluations/add/'. $eval_type . '/' . $eid . '/' . $gid,
                array('query' => drupal_get_destination()));
            }
          }
        }
        else{
          $link = '';
          $conflict = '';
        }
        $content[$gid]['link'] = $link;
      }
    }
    else{
      drupal_set_message(t('Link is true in view but eid not given as argument.'));
    }
  }

  // Evaluations
  if($view['evaluations']){
    foreach ($groups as $gid => $group){
      $evaluation_ids = mkb_eval_get_evaluation_ids($gid);
      if(!empty($evaluation_ids)) $evaluations = l('evaluations', $base_url . '/group_eval/' . $cid . '/' . $gid);
      else $evaluations = "";
      $content[$gid]['evaluations'] = $evaluations;
    }
  }

  // Score only for evaluation type a (quality and expert evaluations)
  if($view['score'] && ($eval_type == 'evaluation_a' || $eval_type == 'evaluation_c')){
    foreach ($groups as $gid => $group){
      if(isset($group['link_evaluation'])){
        $evaluation = $group['link_evaluation'];
        $content[$gid]['score'] = isset($evaluation->field_score['und'][0]['value']) ? $evaluation->field_score['und'][0]['value'] : '';
      }
      else{
        $content[$gid]['score'] = '';
      }
    }
  }

  // Set sessions
  $_SESSION["mkb_eval"] = array(
      'cid' => $cid,
      'vid' => $vid,
      'eid' => isset($eid) ? $eid : '',
      'fid' => isset($fid) ? $fid : '',
      );

  $variables['view_title'] = isset($view['view_title']) ? $view['view_title'] : '';
  $variables['view_description'] = isset($view['view_description']) ? $view['view_description'] : '';
  $variables['count'] = count($content);
  $variables['header'] = $header;
  $variables['content'] = $content;

  return theme('evaluation_page_template', $variables);
}