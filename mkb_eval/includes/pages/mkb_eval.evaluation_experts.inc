<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');

/**
 * Generates content for evaluation_experts_page_template template.
 *
 * @param $cid
 *   Call-main-page nid.
 * @return template
 *   The template evaluation_experts_page_template is called with $variables.
 */
function mkb_eval_evaluation_experts($cid){
  // Load groups
  $groups = mkb_eval_get_stage_2_applications();

  // Header
  $header = array('ID', 'Application', 'Scientific' , 'Sector', 'Rapporteur', 'Status');

  // Images - Conflict
  $con_yes_img = mkb_eval_get_img('con_yes.png', 'Conflict declared', 'Conflict declared');
  $con_no_img = mkb_eval_get_img('con_no.png', 'No conflict declared', 'No conflict declared');
  $con_missing_img = mkb_eval_get_img('con_missing.png', 'Conflict missing', 'Conflict missing');

  // Images - Evaluation
  $eval_missing_img = mkb_eval_get_img('eval_missing.png', 'Evaluation is missing', 'Evaluation is missing');
  $eval_created_img = mkb_eval_get_img('eval_approved.png', 'Evaluation has been created', 'Evaluation has been created');
  $eval_approved_img = mkb_eval_get_img('eval_approved.png', 'Evaluation is approved', 'Evaluation is approved');
  $eval_eval_approved_wc_img = mkb_eval_get_img('eval_approved_wc.png', 'Evaluation is approved with conditions', 'Evaluation is approved with conditions');
  $eval_not_approved_img = mkb_eval_get_img('eval_not_approved.png', 'Evaluation is not approved', 'Evaluation is not approved');

  // Rows
  $rows = array();
  foreach ($groups as $gid => $group){
    // Get expert evaluation
    $expert_evaluation_nid = end(mkb_application_get_group_content($group->nid, 'expert_evaluators'));

    // Get project-info
    $project_info_nids = mkb_application_get_group_content($gid, 'application_project_info');
    $acronym = 'project-info file missing!';
    if($project_info_nids){
      $project_info_node = node_load($project_info_nids[0]);
      $acronym = $project_info_node->field_app_acronym['und'][0]['value'];
    }

    // Set group as link -> expert_evaluators with gid as argument for form
    $group_str = '';
    $scientific_str = 'No experts assigned yet.';
    $sector_str = 'No experts assigned yet.';
    $rapporteur_str = 'No experts assigned yet.';
    $status = '';
    if($expert_evaluation_nid){
      $expert_evaluations = entity_load('node', array($expert_evaluation_nid));
      $expert_evaluation = $expert_evaluations[$expert_evaluation_nid];
      $expert_evaluation_wrapper = entity_metadata_wrapper('node', $expert_evaluation);

      $group_str = l($acronym, 'node/' . $expert_evaluation_wrapper->getIdentifier() . '/edit/' . $group->nid,
          array('query' => drupal_get_destination()));

      $scientific_str = '<ul>';
      $sector_str = '<ul>';
      $rapporteur_str = '<ul>';
      $status = '<b>Complete</b>';
      foreach ($expert_evaluation_wrapper->field_ee_evaluator->getIterator() as $delta => $user_wrapper) {
        // Load user
        $expert = $user_wrapper->value();

          // Conflict
        $status = '<b>Incomplete</b>';
        $conflict_nid = mkb_eval_get_eval_conflict($gid, $expert->uid);
        // Conflict not created yet
        if($conflict_nid) {
          $conflict_node = node_load($conflict_nid);
          if($conflict_node->field_con_conflict['und'][0]['value'] == 1){
            // Conflict declared as yes
            // Link
            $link = l($con_yes_img, 'node/' . $conflict_node->nid . '/edit/' . $gid,
              array(
              'attributes' => array(
                'class' =>'link-image'),
                'html' => true,
                'query' => drupal_get_destination()
              )
            );

            $expert_str = '<li><nobr>' . $expert->realname . ': ' . $link;

            if(in_array('scientific expert', $expert->roles)) $scientific_str .= $expert_str;
            if(in_array('sector expert', $expert->roles)) $sector_str .= $expert_str;
            if(in_array('rapporteur', $expert->roles)) $rapporteur_str .= $expert_str;

          }
          else{
            // Conflict declared as no
            $expert_str = '<li><nobr>' . $expert->realname . ': ' . $con_no_img;
            if(in_array('scientific expert', $expert->roles)) $scientific_str .= $expert_str;
            if(in_array('sector expert', $expert->roles)) $sector_str .= $expert_str;
            if(in_array('rapporteur', $expert->roles)) $rapporteur_str .= $expert_str;
          }
        }
        else{
          // Conflict has not been created
          $expert_str = '<li><nobr>' . $expert->realname . ': ' . $con_missing_img;
          if(in_array('scientific expert', $expert->roles)) $scientific_str .= $expert_str;
          if(in_array('sector expert', $expert->roles)) $sector_str .= $expert_str;
          if(in_array('rapporteur', $expert->roles)) $rapporteur_str .= $expert_str;
        }

          // Evaluations
        // Load evaluation
        $evaluation_ids = mkb_eval_get_exp_evaluation_ids($expert->uid, $group->nid,
            array('evaluation_c', 'evaluation_d', 'evaluation_e'));

        // Status
        if($evaluation_ids){
          // Check if user is rapporteur
          $is_rapporteur = FALSE;
          if(in_array('rapporteur', $expert->roles)){
            $is_rapporteur = TRUE;
          }
          $rapporteur_step = 0;
          $rapporteur_index = 0;
          foreach ($evaluation_ids as $evaluation_id){
            // Each experts evaluation
            $evaluation = evaluation_load($evaluation_id);
            $eval_wrapper = entity_metadata_wrapper('evaluation', $evaluation);

//             $expert_status = '<b>Created</b>';
            $type = $eval_wrapper->getBundle();
            if($type == 'evaluation_c'){
              $recommendation = $evaluation->recommendation();

              if($recommendation == 'Approved'){
                $scientific_str .= ' ' . $eval_approved_img . '</nobr></li>';
              }
              elseif($recommendation == 'Approved with conditions'){
                $scientific_str .= ' ' . $eval_eval_approved_wc_img . '</nobr></li>';
              }
              elseif($recommendation == 'Not approved'){
                $scientific_str .= ' ' . $eval_not_approved_img . '</nobr></li>';
              }

              $rapporteur_step = 1;
              $rapporteur_index++;
            }
            elseif($type == 'evaluation_d'){
              $sector_str .= ' ' . $eval_created_img . '</nobr></li>';
            }
            elseif($type == 'evaluation_e'){
              $rapporteur_str .= ' ' . $eval_created_img . '</nobr></li>';
              $rapporteur_step = 2;
              $rapporteur_index++;
            }
          }
          if($is_rapporteur) {
            if($rapporteur_index != 2){
//               $status = '<b>Incomplete</b>';
              if($rapporteur_step == 1){
                $rapporteur_str .= ' ' . $eval_missing_img . '</nobr></li>';
              }
              elseif($rapporteur_step == 2){
                $scientific_str .= ' ' . $eval_missing_img . '</nobr></li>';
              }
            }
          }
        }
        else{
          // Evaluation has not been created
          $expert_str = ' ' . $eval_missing_img . '</nobr></li>';
          if(in_array('scientific expert', $expert->roles)) $scientific_str .= $expert_str;
          if(in_array('sector expert', $expert->roles)) $sector_str .= $expert_str;
          if(in_array('rapporteur', $expert->roles)) $rapporteur_str .= $expert_str;
        }


      }
      $scientific_str .= '</ul>';
      $sector_str .= '</ul>';
      $rapporteur_str .= '</ul>';
    }
    else{
      $group_str = l($acronym, 'node/add/expert-evaluators/' . $group->nid,
          array('query' => drupal_get_destination()));
    }

    // Add row
    $rows[$gid] = array($group->nid, $group_str, $scientific_str, $sector_str, $rapporteur_str, $status);
//     dpm($rows[$gid]);
  }

  $variables['images'] = array(
      'con_yes_img' => $con_yes_img,
      'con_no_img' => $con_no_img,
      'con_missing_img' => $con_missing_img,
      'eval_missing_img' => $eval_missing_img,
      'eval_created_img' => $eval_created_img,
      'eval_approved_img' => $eval_approved_img,
      'eval_eval_approved_wc_img' => $eval_eval_approved_wc_img,
      'eval_not_approved_img' => $eval_not_approved_img,
      );
  $variables['header'] = $header;
  $variables['rows'] = $rows;
  return theme('evaluation_experts_page_template', $variables);
}

function mkb_eval_get_img($filename, $title, $alt){
  $variables = array(
    'path' => drupal_get_path('module', 'mkb_eval') . '/img/' . $filename,
    'alt' => $alt,
    'title' => $title,
    'width' => '14px',
    'height' => '14px',
    'attributes' => array('class' => 'experts_img'),
  );
  return theme('image', $variables);
}