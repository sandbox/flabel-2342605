<?php
module_load_include('inc', 'mkb_eval', 'includes/mkb_eval.helper');

/**
 * Generates content for group_evaluation_page template.
 *
 * @param $gid
 *   Evaluation configuration node id.
 * @return template
 *   The template group_evaluation_page_template is called with $content.
 */
function evaluation_group_page_function($gid){
  // Info from group
  $group['gid'] = $gid;
  $app_id = mkb_eval_get_aid($gid, 'application_project_info');
  if($app_id){
    $group['acronym'] = mkb_eval_get_acronym($app_id);
    $group['title'] = mkb_eval_get_title($app_id);
  }
  else{
    $group['acronym'] = '';
    $group['title'] = '';
  }

  // Build content from evaluations
  $evaluation_ids = mkb_eval_get_evaluation_ids($gid);
  $content = array();
  foreach ($evaluation_ids as $evaluation_index => $evaluation_id){
    $evaluation = evaluation_load($evaluation_id->evaluation_id);
    $eval_conf = node_load($evaluation->eid);
    $type_letter = substr($evaluation->type, -1);

    // Title = Evaluation type
    $content[$eval_conf->type]['title'] = $eval_conf->title;

    // Data from evaluation
    $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['header'] = array(0 => 'Question', 1 => 'Answer');

    foreach ($evaluation->{'field_answer_' . $type_letter}['und'] as $answer_index => $answer){
      $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['rows'][$answer_index]['question'] = $eval_conf->{'field_eval_q_' . $type_letter}['und'][$answer_index]['value'];
      $answer_value = $answer['value'];
      $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['rows'][$answer_index]['answer'] = $eval_conf->{'field_eval_o_' . $type_letter}['und'][$answer_value]['value'];
    }

    $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['comment'] = isset($evaluation->comment) ? $evaluation->comment : '';
    $author = user_load($evaluation->uid);
    $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['author'] = $author->realname;
    $content[$eval_conf->type]['data'][$evaluation->evaluation_id]['recommendation'] = isset($evaluation->recommendation) ? $evaluation->recommendation() : '';
  }

  $variables['group'] = $group;
  $variables['content'] = $content;
  return theme('evaluation_group_page_template', $variables);
}