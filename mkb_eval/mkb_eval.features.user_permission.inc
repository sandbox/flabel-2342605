<?php
/**
 * @file
 * mkb_eval.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mkb_eval_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create evaluation_conflict content'.
  $permissions['create evaluation_conflict content'] = array(
    'name' => 'create evaluation_conflict content',
    'roles' => array(
      'call administrator' => 'call administrator',
      'rapporteur' => 'rapporteur',
      'scientific expert' => 'scientific expert',
      'sector expert' => 'sector expert',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create rapporteur_evaluation content'.
  $permissions['create rapporteur_evaluation content'] = array(
    'name' => 'create rapporteur_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create scientific_evaluation content'.
  $permissions['create scientific_evaluation content'] = array(
    'name' => 'create scientific_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create sector_evaluation content'.
  $permissions['create sector_evaluation content'] = array(
    'name' => 'create sector_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any evaluation_conflict content'.
  $permissions['delete any evaluation_conflict content'] = array(
    'name' => 'delete any evaluation_conflict content',
    'roles' => array(
      'administrator' => 'administrator',
      'call administrator' => 'call administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any rapporteur_evaluation content'.
  $permissions['delete any rapporteur_evaluation content'] = array(
    'name' => 'delete any rapporteur_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any scientific_evaluation content'.
  $permissions['delete any scientific_evaluation content'] = array(
    'name' => 'delete any scientific_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any sector_evaluation content'.
  $permissions['delete any sector_evaluation content'] = array(
    'name' => 'delete any sector_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own evaluation_conflict content'.
  $permissions['delete own evaluation_conflict content'] = array(
    'name' => 'delete own evaluation_conflict content',
    'roles' => array(
      'administrator' => 'administrator',
      'call administrator' => 'call administrator',
      'rapporteur' => 'rapporteur',
      'scientific expert' => 'scientific expert',
      'sector expert' => 'sector expert',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own rapporteur_evaluation content'.
  $permissions['delete own rapporteur_evaluation content'] = array(
    'name' => 'delete own rapporteur_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own scientific_evaluation content'.
  $permissions['delete own scientific_evaluation content'] = array(
    'name' => 'delete own scientific_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own sector_evaluation content'.
  $permissions['delete own sector_evaluation content'] = array(
    'name' => 'delete own sector_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any evaluation_conflict content'.
  $permissions['edit any evaluation_conflict content'] = array(
    'name' => 'edit any evaluation_conflict content',
    'roles' => array(
      'administrator' => 'administrator',
      'call administrator' => 'call administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any rapporteur_evaluation content'.
  $permissions['edit any rapporteur_evaluation content'] = array(
    'name' => 'edit any rapporteur_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any scientific_evaluation content'.
  $permissions['edit any scientific_evaluation content'] = array(
    'name' => 'edit any scientific_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any sector_evaluation content'.
  $permissions['edit any sector_evaluation content'] = array(
    'name' => 'edit any sector_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own evaluation_conflict content'.
  $permissions['edit own evaluation_conflict content'] = array(
    'name' => 'edit own evaluation_conflict content',
    'roles' => array(
      'administrator' => 'administrator',
      'call administrator' => 'call administrator',
      'rapporteur' => 'rapporteur',
      'scientific expert' => 'scientific expert',
      'sector expert' => 'sector expert',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own rapporteur_evaluation content'.
  $permissions['edit own rapporteur_evaluation content'] = array(
    'name' => 'edit own rapporteur_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own scientific_evaluation content'.
  $permissions['edit own scientific_evaluation content'] = array(
    'name' => 'edit own scientific_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own sector_evaluation content'.
  $permissions['edit own sector_evaluation content'] = array(
    'name' => 'edit own sector_evaluation content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
