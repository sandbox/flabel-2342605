<?php
/**
 * @file
 * mkb_eval.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mkb_eval_user_default_roles() {
  $roles = array();

  // Exported role: rapporteur.
  $roles['rapporteur'] = array(
    'name' => 'rapporteur',
    'weight' => 9,
  );

  // Exported role: scientific expert.
  $roles['scientific expert'] = array(
    'name' => 'scientific expert',
    'weight' => 7,
  );

  // Exported role: sector expert.
  $roles['sector expert'] = array(
    'name' => 'sector expert',
    'weight' => 8,
  );

  return $roles;
}
